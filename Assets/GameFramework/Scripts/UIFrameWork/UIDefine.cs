﻿using UnityEngine;
using System.Collections;

public class UIDefine
{
    public class Path
    {
        public const string UIFixedPanelPath = "";
        public const string UINormalPanelPath = "";
        public const string UIWindowPath = "";
        public const string UIWidget = "";
    }
    
}

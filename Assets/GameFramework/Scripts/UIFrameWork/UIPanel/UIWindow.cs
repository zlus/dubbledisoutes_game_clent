﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
/// <summary>
/// UI窗体，采用堆栈管理
/// </summary>
public class UIWindow : UIBasePanel
{
    public delegate void CloseEvent(object arg = null);

    /// <summary>
    /// 关闭按钮
    /// </summary>
    [SerializeField]
    private ZoomButton m_btnClose;
    /// <summary>
    /// 窗口关闭事件
    /// </summary>
    public event CloseEvent onClose;
    /// <summary>
    /// 打开UI的参数
    /// </summary>
    protected object m_openArg;
    /// <summary>
    /// 该UI的当前实例是否被打开过
    /// </summary>
    private bool m_isOpenedOnce;

    protected void OnEnable()
    {
        if (m_btnClose != null)
        {
            m_btnClose.onClick.AddListener(()=> { OnBtnClose(); });
        }
    }

    public CloseEvent RegisterOnClose
    {
        set
        {
            onClose = null;
            onClose += value;
        }

    }

    protected void OnDisable()
    {
        if (m_btnClose != null)
        {
            m_btnClose.onClick.RemoveAllListeners();
        }
        
    }
    
    protected void OnBtnClose(object arg=null)
    {
        UIManager.Instance.CloseWindow(arg);
    }
    public sealed override void Open(object arg = null)
    {
        m_openArg = arg;
        m_isOpenedOnce = false;
        this.transform.localScale = new Vector3(0, 0, 1);
        if (!this.gameObject.activeSelf)
        {
            
            this.gameObject.SetActive(true);
            
        }
        this.transform.DOScale(new Vector3(1, 1, 1), 0.2f).SetEase(Ease.OutBack);
        OnOpen(arg);
        m_isOpenedOnce = true;
    }
    public sealed override void Close(object arg = null)
    {

        this.transform.DOScale(new Vector3(1, 1, 1), 0.2f).SetEase(Ease.InBack);
        this.gameObject.SetActive(false);

        //OnClose(arg);
        if (onClose != null)
        {
            onClose(arg);
            
        }
    }
}

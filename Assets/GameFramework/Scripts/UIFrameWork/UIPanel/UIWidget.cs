﻿using UnityEngine;
using System.Collections;
/// <summary>
/// UI挂件，比如一些弹屏，滚动条，提示等
/// </summary>
public class UIWidget : UIBasePanel
{
    protected object m_openArg;

    public sealed override void Open(object arg = null)
    {
        base.Open(arg);
        m_openArg = arg;
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
        OnOpen(arg);
    }

    public sealed override void Close(object arg = null)
    {
        base.Close(arg);
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
        OnClose(arg);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//TODO继承单例
public class UIManager:Singleton<UIManager>
{
    /// <summary>缓存的Panel集合 </summary>
    private Dictionary<string, UIBasePanel> _UIPanelCacheDic;
    /// <summary>当前在界面上显示的Panel集合 </summary>
    private Dictionary<string, UIBasePanel> _UIPanelCurrentShowDic;
    /// <summary>窗体Panel的栈 </summary>
    private Stack<UIWindow> m_UIWindowsStack;
    private Transform collisionMask;

    protected override void InitSingleton() {
        base.InitSingleton();
        _UIPanelCacheDic = new Dictionary<string, UIBasePanel>();
        _UIPanelCurrentShowDic = new Dictionary<string, UIBasePanel>();
        m_UIWindowsStack = new Stack<UIWindow>();
    }

    public Transform CollisionMask
    {
        get
        {
            if (collisionMask != null) return collisionMask;
            collisionMask = GameObject.Instantiate(UIRes.LoadPrefab("CollisionMask")).transform;
            collisionMask.SetParent(UIRoot.PopUpNode, false);
            collisionMask.transform.name = "CollisionMask";
            return collisionMask;
        }
    }
    public void Init()
    {
        //collisionMask = new GameObject();
        //RectTransform maskTransform= collisionMask.AddComponent<RectTransform>();
        //collisionMask.AddComponent<CanvasGroup>();
        //maskTransform.pivot = new Vector2(0.5f, 0.5f);
        //maskTransform.anchorMin = new Vector2(0, 0);
        //maskTransform.anchorMax = new Vector2(1, 1);

    }
    public void ClearAll()
    {
        _UIPanelCacheDic.Clear();
        _UIPanelCurrentShowDic.Clear();
        m_UIWindowsStack.Clear();
        AddMask(-2);
    }

    public void ClearUIPanelCurrentShowDic( bool isThorough) {
        if(isThorough) {
            foreach(var temp in _UIPanelCurrentShowDic.Keys) {
                UnLoad<UIBasePanel>(temp);
            }
        }
    }

    /// <summary>
    /// 清理缓存数据
    /// </summary>
    public void ClearCacheDic() {
        if (_UIPanelCacheDic.Count == 0) return;
        foreach(var temp in _UIPanelCacheDic.Values) {
            temp.DestorySelf();
        }
        _UIPanelCacheDic.Clear();
    }
    //根据路径加载UIPanel
    private T Load<T>(string path) where T : UIBasePanel
    {
        UIBasePanel ui = null;
        //1.如果该UIPanel在当前界面已经显示则返回空
        if (_UIPanelCurrentShowDic.ContainsKey(path)) return null;
        //2.无法获取，则从panel缓存中获取panel
        _UIPanelCacheDic.TryGetValue(path, out ui);
        if (ui == null)
        {
            //3.无法获取则，则加载
            GameObject original = UIRes.LoadPrefab(path);
            if (original != null)
            {
                GameObject go = GameObject.Instantiate(original);
                ui = go.GetComponent<T>();
                if (ui != null)
                {
                    int index = path.LastIndexOf("/") + 1;
                    ui.name = path.Substring(index);
                    //UIRoot.AddChild(ui);
                    //if (_UIPanelCurrentShowDic.ContainsKey(path)) {
                    //    path
                    //        }
                    _UIPanelCurrentShowDic.Add(path, ui);
                    return ui as T;
                }
                return null;
            }
            
        }
        else
        {
            _UIPanelCacheDic.Remove(path);
            _UIPanelCurrentShowDic.Add(path, ui);
        }

        return ui as T;
    }

    private T UnLoad<T>(string path,object arg = null)
        where T :UIBasePanel
    {
        UIBasePanel ui = null;
        if (_UIPanelCurrentShowDic.ContainsKey(path)){
            _UIPanelCurrentShowDic.TryGetValue(path, out ui);
            if(ui!=null) {
                _UIPanelCacheDic.Add(path, ui);
                _UIPanelCurrentShowDic.Remove(path);
                ui.Close(arg);
            }

            return ui as T;
        }
        return default(T);
        
    }

    private T Open<T>(string path, object arg = null) where T : UIBasePanel
    {
        T ui = Load<T>(path);
        if (ui != null)
        {
            ui.transform.SetAsLastSibling();
            ui.Open(arg);
        }
        else
        {
            Debug.LogError("non obj been load [ path : " + path + " ]");
        }
        return ui;
    }

    private void CloseAllPanel()
    {
        foreach (var panel in _UIPanelCurrentShowDic.Values)
        {
            if (panel.IsOpen)
            {
                panel.Close();
            }
        }
        _UIPanelCurrentShowDic.Clear();
        m_UIWindowsStack.Clear();
    }


    //========================NormalPanel==============================
    #region NormalPanel
    public UINormalPanel OpenNormalPanel(string name, object arg = null)
    {
        string path = UIDefine.Path.UINormalPanelPath + name;
        UINormalPanel ui= Open<UINormalPanel>(name, arg);
        ui.transform.SetParent(UIRoot.NormalNode, false);
        return ui;
    }

    public UINormalPanel CloseNormalPanel(string name, object arg = null)
    {
        string path = UIDefine.Path.UINormalPanelPath + name;
        //UIBasePanel normalPanel = null;
        //_UIPanelCurrentShowDic.TryGetValue(path, out normalPanel);
        //if (normalPanel != null)
        //{
        //    _UIPanelCurrentShowDic.Remove(path);
        //    _UIPanelCacheDic.Add(path, normalPanel);
        //    normalPanel.Close();
        //    return normalPanel as UINormalPanel;
        //}
        //return null;
        return UnLoad<UINormalPanel>(path);
    }
    #endregion

    //===========================Window================================
    #region Window

    public UIWindow OpenWindow(string name, object arg = null)
    {
        string path = UIDefine.Path.UIWindowPath + name;
        UIWindow window = Open<UIWindow>(path,arg);
        
        if (window != null)
        {
            window.transform.SetParent(UIRoot.PopUpNode, false);
            int index = window.transform.GetSiblingIndex() - 1;
            if (m_UIWindowsStack == null) m_UIWindowsStack = new Stack<UIWindow>();
            m_UIWindowsStack.Push(window);
            AddMask(index);
            return window;
            
        }
        return null;
    }

    public UIWindow CloseWindow(object arg = null)
    {
        if (m_UIWindowsStack == null) return null;
        UIWindow window= m_UIWindowsStack.Pop();
        if (window ==null) return null;
        

        if (m_UIWindowsStack.Count == 0)
        {
            AddMask(-2);
        }
        else
        {

            UIWindow topWindow = m_UIWindowsStack.Peek();
            AddMask(topWindow.transform.GetSiblingIndex());
        }
        UnLoad<UIWidget>(window.name, arg);
        return window;
    }

    #endregion

    //===========================Widget================================
    #region Widget
    public UIWidget OpenWidget(string name, object arg = null)
    {
        string path = UIDefine.Path.UIWidget + name;
        UIWidget ui= Open<UIWidget>(name, arg);
        ui.transform.SetParent(UIRoot.WidgetNode, false);
        return ui;
    }
    public UIWidget CloseWidget(string name,object arg = null)
    {
        string path = UIDefine.Path.UIWidget + name;
        return UnLoad<UIWidget>(name);

    }
    #endregion

    public void AddMask(int index)
    {
        CanvasGroup canvasGroup = CollisionMask.GetComponent<CanvasGroup>();
        if (index == -2)
        {
            canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0;
            CollisionMask.SetSiblingIndex(0);
        }
        else
        {
            canvasGroup.blocksRaycasts = true;
            canvasGroup.alpha = 0.5f;
            if (index < 0)
            {

                CollisionMask.SetAsFirstSibling();
            }else
            CollisionMask.SetSiblingIndex(index);
        }

    }
}

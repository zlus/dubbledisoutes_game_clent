﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//所有类型UI面板的基类
public class UIBasePanel : MonoBehaviour{

    /// <summary>
    /// 当前UI是否打开
    /// </summary>
    public bool IsOpen { get { return this.gameObject.activeSelf; } }
    #region unity回调
    private void Awake()
    {

        OnAwake();
    }
    private void OnDestroy()
    {

        BeforeDestroy();
    }
    #endregion

    public void DestorySelf() {
        GameObject.Destroy(this.gameObject);
    }

    protected virtual void OnAwake()
    {

    }

    protected virtual void BeforeDestroy()
    {

    }
    /// <summary>
    ///框架内调用 
    /// </summary>
    public virtual void Open(object arg = null)
    {

    }

    /// <summary>
    /// 框架内调用
    /// </summary>
    public virtual void Close(object arg = null)
    {

    }

    /// <summary>
    /// 子类继承
    /// </summary>
    protected virtual void OnClose(object arg = null)
    {

    }

    /// <summary>
    /// 子类继承
    /// </summary>
    protected virtual void OnOpen(object arg = null)
    {

    }
    #region 封装的方法

    public T FindInChild<T>(string name) where T : MonoBehaviour
    {
        GameObject obj = FindInChild(name);
        if (obj != null)
            return obj.GetComponent<T>();
        return null;
    }
    public GameObject FindInChild(string name)
    {
        Transform obj = null;
        obj = this.transform.Find(name);
        if (obj != null)
            return obj.gameObject;
        return null;
    }
    #endregion
}

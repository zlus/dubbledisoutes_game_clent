﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 一个场景中的固定UI面板元素
/// </summary>
public class UIFixedPanel : UIBasePanel
{
    protected object m_openArg;

    public sealed override void Open(object arg = null)
    {
        base.Open(arg);
        m_openArg = arg;
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
        OnOpen(arg);
    }
    public sealed override void Close(object arg = null)
    {
        base.Close(arg);
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
        OnClose(arg);
    }
}

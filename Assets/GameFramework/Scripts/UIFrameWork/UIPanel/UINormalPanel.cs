﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 普通UI面板
/// </summary>
public class UINormalPanel : UIBasePanel
{
    protected object m_openArg;

    public sealed override void Open(object arg = null)
    {
        base.Open(arg);
        m_openArg = arg;
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
        OnOpen(arg);
    }

    public sealed override void Close(object arg = null)
    {
        base.Close(arg);
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
        OnClose(arg);
    }
}

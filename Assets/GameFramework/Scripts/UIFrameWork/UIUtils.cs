﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public static class UIUtils
{
    /// <summary>
    /// 设置一个UI元素是否可见
    /// </summary>
    /// <param name="ui"></param>
    /// <param name="value"></param>
    public static void SetActive(UIBehaviour ui, bool value)
    {
        if (ui != null && ui.gameObject != null)
        {
            GameObjectUtils.SetActiveRecursively(ui.gameObject, value);
        }
    }


    public static void SetButtonText(ZoomButton btn, string text)
    {
        Text objText = btn.transform.GetComponentInChildren<Text>();
        if (objText != null)
        {
            objText.text = text;
        }
    }

    public static string GetButtonText(ZoomButton btn)
    {
        Text objText = btn.transform.GetComponentInChildren<Text>();
        if (objText != null)
        {
            return objText.text;
        }
        return "";
    }

    public static void SetChildText(UIBehaviour ui, string text)
    {
        Text objText = ui.transform.GetComponentInChildren<Text>();
        if (objText != null)
        {
            objText.text = text;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 排序类型
/// </summary>
public enum SortType
{
    /// <summary>
    /// 向前排序最前
    /// </summary>
    Before=0,
    /// <summary>
    /// 向后排序
    /// </summary>
    Next=1
}
public static class UIRoot
{
    private static Dictionary<string, UIBasePanel> UIPanelDic = new Dictionary<string, UIBasePanel>();

    private static GameObject m_UIRoot;
    private static Transform m_NormalNode;
    private static Transform m_FixedNode;
    private static Transform m_PopUpNode;
    private static Transform m_WigetNode;
    
    public static GameObject M_Canvas
    {
        get
        {
            if (m_UIRoot == null) {
                m_UIRoot = GameObject.Find("Canvas");
                if (m_UIRoot == null)
                {
                    m_UIRoot=GameObject.Instantiate<GameObject>(UIRes.LoadPrefab("UI/Canvas"));
                    m_UIRoot.name = "Canvas";
                }
            }
            return m_UIRoot;
        }
    }
    public static Transform NormalNode
    {
        get
        {
            if (m_NormalNode == null)
            {
                m_NormalNode = FindInChild("NormalNode").transform;
            }
            return m_NormalNode;
        }
    }
    public static Transform FixedNode
    {
        get
        {
            if (m_FixedNode == null)
            {
                m_FixedNode = FindInChild("FixedNode").transform;
            }
            return m_FixedNode;
        }
    }
    public static Transform PopUpNode
    {
        get
        {
            if (m_PopUpNode == null)
            {
                m_PopUpNode = FindInChild("PopUpNode").transform;
            }
            return m_PopUpNode;
        }
    }
    public static Transform WidgetNode
    {
        get
        {
            if (m_WigetNode == null)
            {
                m_WigetNode = FindInChild("WigetNode").transform;
            }
            return m_WigetNode;
        }
    }


    public static T FindUiPanel<T>(string name)
        where T :UIBasePanel
    {
        if (UIPanelDic.ContainsKey(name))
        {
            T temp = UIPanelDic[name] as T;
            return temp;
        }
        return null;
    }

    public static T FindInChild<T>() where T : MonoBehaviour
    {
        string name = typeof(T).Name;
        return FindInChild<T>(name);
    }
    public static T FindInChild<T>(string name) where T : MonoBehaviour
    {
        GameObject obj = FindInChild(name);
        if (obj != null)
            return obj.GetComponent<T>();
        return null;
    }
    public static GameObject FindInChild(string name)
    {
        Transform obj=null;
        if (M_Canvas != null)
        {
            obj=M_Canvas.transform.Find(name);
        }
        if (obj != null)
            return obj.gameObject;
        return null;

    }

    private static int UISortingMax = 0;
    private static int UISortingMin = 0;
    /// <summary>
    /// 添加Panel到UIRoot下
    /// </summary>
    /// <param name="child"></param>
    /// <param name="sortType"></param>
    public static void AddChild(UIBasePanel child,SortType sortType=SortType.Next)
    {
        GameObject root = M_Canvas;
        if (root == null || child == null)
        {
            return;
        }
        child.transform.SetParent(root.transform, false);
        
    }
}

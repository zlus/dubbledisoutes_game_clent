﻿using UnityEngine;
using System.Collections;
using System;
public static class UIRes
{
    public static GameObject LoadPrefab(string path)
    {
     //   Debug.Log(AppConfig.rootPath + "/UI/Prefab/" + path);
        GameObject asset = ResManager.Instance.LoadGameObjectAsset(AppConfig.rootPath + "UI/Prefab/" + path);
        return asset;
    }
}

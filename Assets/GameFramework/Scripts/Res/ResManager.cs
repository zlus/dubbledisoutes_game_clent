#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            AssetManager.cs 
/// 作者(Author):                 Zlu
/// 创建时间(CreateTime):          2017/11/10 11:17:18
/// 修改者列表(Modifier):
/// 模块描述(Module description):
/// 更新时间(Update Time):
/// </summary>
#endregion
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ResManager : ServiceModule<ResManager> {


    public override void Init() {
        AssetBundleAdapter.LoadManifest();
    }


    #region 本地加载方法
    /// <summary>
    /// 从Resource中获取资源
    /// </summary>
    /// <param name="path">资源路径</param>
    /// <returns></returns>
    public T LoadAssetFromResource<T>(string path)
        where T : UnityEngine.Object {
        return Resources.Load<T>(path);
    }
    /// <summary>
    /// 加载AssetBundle
    /// </summary>
    /// <param name="BundleName">资源包名</param>
    /// <returns></returns>
    public void  LoadAssetBundle(string BundleName) {
#if !UNITY_EDITOR
        AssetBundleAdapter.LoadAB(BundleName);
#endif
    }
    /// <summary>
    /// 异步加载AssetBundle
    /// </summary>
    /// <param name="abPath"></param>
    /// <param name="onDownloadProgressUpdate"></param>
    public void LoadAssetBundleAsync(string abPath, OnDownloadProgressUpdate onDownloadProgressUpdate) {
#if!UNITY_EDITOR
        GameSystem.Instance.StartCoroutine(AssetBundleAdapter.LoadABAsync(abPath, onDownloadProgressUpdate));
#endif
        }
    /// <summary>
    /// 加载资源
    /// </summary>
    /// <param name="BundleName">包名</param>
    /// <param name="AssetName">资源名</param>
    /// <returns></returns>
    public T LoadAsset<T>(string AssetName)
        where T : UnityEngine.Object {
#if UNITY_EDITOR
        return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(AssetName);
#else
        return AssetBundleAdapter.LoadAsset<T>(AssetName);
#endif


    }
    /// <summary>
    /// 加载Sub资源
    /// </summary>
    /// <param name="AssetName">资源名</param>
    /// <returns></returns>
    public T[] LoadSubAssets<T>(string AssetName)
        where T : Object {
#if UNITY_EDITOR
        return UnityEditor.AssetDatabase.LoadAllAssetsAtPath(AssetName) as T[];
#else
        return AssetBundleAdapter.LoadAssetWithSubAssets<T>(AssetName);
#endif

    }
    //#if UNITY_EDITOR
    //#else
    //#endif
    public void UnLoad(UnityEngine.Object asset) {
#if !UNITY_EDITOR
        AssetBundleAdapter.UnLoad(asset);
#endif
    }
    public void UnLoadBundle(string bundleName) {
#if !UNITY_EDITOR
        AssetBundleAdapter.UnLoadBundle(bundleName);
#endif
    }

    /// <summary>
    /// 加载GameObject资源
    /// </summary>
    /// <param name="AssetName">资源名</param>
    /// 
    public GameObject LoadGameObjectAsset(string AssetName) {
        return LoadAsset<GameObject>(AssetName+".Prefab");
    }

    /// <summary>
    /// 加载Texture2D资源，如果指定了AssetBundle包名就从AssetBundle中加载，如未指定则从Resoures中加载
    /// </summary>
    /// <param name="AssetName">资源名</param>
    /// <param name="BundleName">包名</param>
    public Texture2D LoadTexture2DAsset(string AssetName) {

        return LoadAsset<Texture2D>(AssetName+".png");

    }

    public Sprite LoadSpriteAsset(string assetName)
    {
        return LoadAsset<Sprite>(assetName+".png");
    }

    /// <summary>
    /// 加载AudioClip资源，如果指定了AssetBundle包名就从AssetBundle中加载，如未指定则从Resoures中加载
    /// </summary>
    /// <param name="AssetName">资源名</param>
    /// <param name="BundleName">包名</param>
    public AudioClip LoadAudioClipAsset(string AssetName) {

        return LoadAsset<AudioClip>(AssetName);

    }
#endregion

}

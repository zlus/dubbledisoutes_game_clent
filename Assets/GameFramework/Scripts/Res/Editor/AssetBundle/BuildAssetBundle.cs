﻿/*=================================================================
* Author: Zlu
* CreatedTime: 11/16/2017 6:26:47 PM
* Description: <功能描述> 
===================================================================*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;


public static class BuildAssetBundle {

    public static string root_dir = Application.streamingAssetsPath + "/AssetBundle/";
    public static string filePath = Application.streamingAssetsPath;
    public static void ExportResourcesManifestFile() {

        string manifestPath = Application.streamingAssetsPath + "/AssetBundle/"
                + AssetbundleUtil.AssetBundle_FileName;
        AssetBundleManifest Manifest = null;
        if (!System.IO.File.Exists(manifestPath)) {
            return;
        }
        AssetBundle manifest_bundle = UnityEngine.AssetBundle.LoadFromFile(manifestPath);
        if (manifest_bundle != null) {
            Manifest = manifest_bundle.LoadAsset("AssetBundleManifest") as AssetBundleManifest;
            manifest_bundle.Unload(false);
        }
        if (Manifest != null) {

            List<AssetBundleInfo> abInfoList = new List<AssetBundleInfo>();

            foreach (var name in Manifest.GetAllAssetBundles()) {
                AssetBundleInfo abInfo = new AssetBundleInfo();
                abInfo.name = name;
                Debug.Log(root_dir+name);
                AssetBundle ab = AssetBundle.LoadFromFile(root_dir+name);
                foreach(var asset in ab.GetAllAssetNames()) {
                    abInfo.assets.Add(asset);
                }
                abInfoList.Add(abInfo);
                ab.Unload(false);
            }
            string abInfoSl= JsonMapper.ToJson(abInfoList);
            IOUtil.CreateTextFile(filePath+"/"+"AssetBundleInfo.txt", abInfoSl);
            UnityEditor.AssetDatabase.Refresh();
        }
    }


}

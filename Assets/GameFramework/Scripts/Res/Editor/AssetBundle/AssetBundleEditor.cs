using System;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetBundleEditor : Editor {
    /// <summary>
    /// 打包所有的AssetsBundle
    /// </summary>
	[MenuItem("AssetBundle/BuildAll/Windows")]
    static void BuildAllAssetBundleWindows() {


        BuildPipeline.BuildAssetBundles(BuildPlatformAssetBundleDir("Windows"), BuildAssetBundleOptions.None
            , BuildTarget.StandaloneWindows64);
        AssetDatabase.Refresh();
    }

    [MenuItem("AssetBundle/BuildAll/Android")]
    static void BuidAllAssetBundleAndroid() {
        BuildPipeline.BuildAssetBundles(BuildPlatformAssetBundleDir("Android"), BuildAssetBundleOptions.None
            , BuildTarget.Android);
        AssetDatabase.Refresh();
    }
    [MenuItem("AssetBundle/BuildAll/IOS")]
    static void BuidAllAssetBundleIOS() {
        BuildPipeline.BuildAssetBundles(BuildPlatformAssetBundleDir("IOS"), BuildAssetBundleOptions.None
            , BuildTarget.iOS);
        AssetDatabase.Refresh();
    }

    public static string  BuildPlatformAssetBundleDir(string PlatformName) {

        string Path = Application.streamingAssetsPath + "/AssetBundle";
        if (Directory.Exists(Path)) {
            Directory.Delete(Path,true);
        }
        string outPath = Application.streamingAssetsPath + "/AssetBundle/";// + AssetbundleUtil.GetPlatformFolderForAssetBundles(Application.platform);
        Directory.CreateDirectory(outPath);
        AssetDatabase.Refresh();
        return outPath;
    }
    /// <summary>
    /// 打包当前选择文件夹或则文件的AssetBundle
    /// </summary>
    static void BuildSeletedAssetsBundle() {

    }

    //[MenuItem("AssetBundle/test")]
    //static void test() {
    //    Debug.Log(AssetBundleConfig.Project_Path);
    //}
    /// <summary>
    /// 
    /// </summary>
    [MenuItem("AssetBundle/SetAssetBundleName")]
    static void SetResourcesAssetBundleName() {
        AssetDatabase.RemoveUnusedAssetBundleNames();
        //需要命名的资源后缀名
        //string[] FilterSuffix = new string[] { ".prefab", ".mat", ".dds", ".psd",".png" };
        //UnityEngine.Object[] SelectedAsset = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets);
        //if (!(SelectedAsset.Length == 1)) {         //如果没有选中文件夹，返回
        //    return;
        //}

        AssetBundleDAL ABDal = new AssetBundleDAL();

        Debug.Log(ABDal.GetList()[0].Tag);

        foreach (AssetBundleEntity ABEntity in ABDal.GetList()) {
            if (!ABEntity.IsFolder) {

                foreach (var path in ABEntity.PathList) {
                    
                    AssetImporter importer = AssetImporter.GetAtPath("Assets/" + path);
                    importer.assetBundleName = ABEntity.Tag;
                }
               

            } else {
                foreach (var path in ABEntity.PathList) {
                    string _path = (AssetbundleUtil.Application_Path + path).Replace('\\', '/');
                    Debug.Log(_path);
                    DirectoryInfo dirInfo = new DirectoryInfo(_path);
                    var files = dirInfo.GetFiles("*", SearchOption.AllDirectories);
                    Debug.Log(files.Length);
                    for (int i = 0; i < files.Length; i++) {
                        var fileInfo = files[i];
                        if (fileInfo.Extension.Equals(".meta", StringComparison.CurrentCultureIgnoreCase)) continue;
                        //EditorUtility.DisplayCancelableProgressBar(
                        //    "设置AssetBundle名称", "正在设置AssetBundle名称...", 1f * i / files.Length);
                        //Unity路径用的是\，Windows用的是/，统一为/（Unity都能识别）
                        string Fpath = fileInfo.FullName.Replace('\\', '/').Substring(fileInfo.FullName.IndexOf("Assets"));
                        Debug.Log("Fpath: "+Fpath);
                        var importer = AssetImporter.GetAtPath(Fpath);
                        if (importer != null) importer.assetBundleName = ABEntity.Tag;
                    }

                }
            }

        }

        EditorUtility.ClearProgressBar();
    }

    [MenuItem("test/tt")]
    static void test() {
        Debug.Log(Application.platform);
    }

    /// <summary>
    /// 清空AssetBundle名字
    /// </summary>
    [MenuItem("AssetBundle/ClearAssetBundleName")]
    static void ClearResourcesAssetBundleName() {

        UnityEngine.Object[] SelectedAsset = Selection.GetFiltered(typeof(UnityEngine.Object),
                                SelectionMode.Assets);

        
        if (!(SelectedAsset.Length == 1)) return;
        
        string fullPath = (AssetbundleUtil.Project_Path + AssetDatabase.GetAssetPath(SelectedAsset[0])).Replace("\\","/");
        Debug.Log(fullPath);
        if (Directory.Exists(fullPath)) {
            
            DirectoryInfo dir = new DirectoryInfo(fullPath);

            var files = dir.GetFiles("*", SearchOption.AllDirectories);
            ;
            for (var i = 0; i < files.Length; ++i) {
                var fileInfo = files[i];
                EditorUtility.DisplayProgressBar("清除AssetName名称",
                "正在清除AssetName名称中...", 1f * i / files.Length);

                string path = fileInfo.FullName.Replace('\\',
                '/').Substring(AssetbundleUtil.Project_Path.Length);
                var importer = AssetImporter.GetAtPath(path);
                if (importer) {
                    importer.assetBundleName = null;
                }

            }
        }
        EditorUtility.ClearProgressBar();
        AssetDatabase.RemoveUnusedAssetBundleNames();
    }
    [MenuItem("AssetBundle/BuildManifestInfo")]
    static void BuildManifestInfo() {
        BuildAssetBundle.ExportResourcesManifestFile();
    }
}

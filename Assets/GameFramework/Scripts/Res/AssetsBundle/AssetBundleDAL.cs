#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            AssetBundleDAL.cs 
/// 作者(Author):                 Zlu
/// 创建时间(CreateTime):          2017/11/9 14:40:47
/// 修改者列表(Modifier):
/// 模块描述(Module description):
/// 更新时间(Update Time):
/// </summary>
#endregion
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class AssetBundleDAL {

    private string m_Path;//xml路径
    private List<AssetBundleEntity> m_List = null;//数据集合
    private Dictionary<string, List<string>> map_Tag_Path = new Dictionary<string, List<string>>();

    public Dictionary<string, List<string>> Map_Tag_Path {
        get {
            GetList();
            return map_Tag_Path;
        }
    }

    public AssetBundleDAL() {
        m_Path = AssetbundleUtil.AssetBundleXMLConfigPath;
        m_List = new List<AssetBundleEntity>();
    }
    /// <summary>
    /// 返回Xml数据
    /// </summary>
    /// <returns></returns>
    public List<AssetBundleEntity> GetList() {
        m_List.Clear();

        //读取xml 把数据添加到m_List 里面
        XDocument xDoc = XDocument.Load(m_Path);
        XElement root = xDoc.Root;
        XElement assetBundleNode = root.Element("AssetBundle");
        IEnumerable<XElement> lst = assetBundleNode.Elements("Item");
        int index = 0;
        foreach (XElement item in lst) {
            AssetBundleEntity entity = new AssetBundleEntity();
            entity.Key = "key" + ++index;
            entity.Name = item.Attribute("Name").Value;
            entity.Tag = item.Attribute("Tag").Value;//包名
            entity.IsFolder = item.Attribute("IsFolder").Value.Equals("True", System.StringComparison.CurrentCultureIgnoreCase);
            entity.IsFirstData = item.Attribute("IsFirstData").Value.Equals("True", System.StringComparison.CurrentCultureIgnoreCase);

            IEnumerable<XElement> pathList = item.Elements("Path");
            List<string> m_pathList = new List<string>();
            foreach (XElement path in pathList) {
                entity.PathList.Add(path.Attribute("Value").Value);//路径
                m_pathList.Add(path.Attribute("Value").Value);
            }
            if(!map_Tag_Path.ContainsKey(entity.Tag))
            map_Tag_Path.Add(entity.Tag, m_pathList);
            m_List.Add(entity);
        }

        return m_List;
    }
}

#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            AssetsBundleConfig.cs 
/// 作者(Author):                 Syman
/// 创建时间(CreateTime):          2017/9/15 11:35:33
/// 修改者列表(Modifier):
/// 模块描述(Module description):              AssetsBundle配置信息
/// 更新时间(Update Time):
/// </summary>
#endregion
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

//using UnityEditor;
public class AssetbundleUtil {
    //平台打包
    public static readonly string PathURL =
#if UNITY_ANDROID
        "jar:file://" + Application.dataPath + "!/assets/";
#elif UNITY_IPHONE
    Application.dataPath + "/Raw/";
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR
     Application.dataPath + "/StreamingAssets/";
#else
    string.Empty;
#endif

    public static string AssetBundleXMLConfigPath = Application.dataPath + "/GameFramework/Config/AssetBundleConfig.xml";

    //AssetBundle打包绝对路径,例如：E:/WorkSpaceUnity5.5.0/AssetBundleText/Assets/StreamingAssets/AssetBundle
    public static string AssetBundle_AbsolutePath = Application.dataPath + "/StreamingAssets/AssetBundle/";
    //AssetBundle打包相对路径,例如：StreamingAssets/AssetBundle
    public static string AssetBundle_RelativePath = "Assets/StreamingAssets/AssetBundle";
    //E:/WorkSpaceUnity5.5.0/BreadMenAll/BreadMenNew/Assets/
    public static string Application_Path = Application.dataPath + "/";
    //E:/WorkSpaceUnity5.5.0/BreadMenAll/BreadMenNew/
    public static string Project_Path = Application_Path.Substring(0, Application_Path.Length - 7);
    //AssetBundle存放的文件夹名
    public static string AssetBundle_FileName = "AssetBundle";
    //AssetBundle打包的后缀名
    public static string Suffix = ".unity3d";

    public static string Path = Application.streamingAssetsPath;

    public static string SavePath = Application.persistentDataPath + "/AssetBundle/";

    private static string assetBundleLocalDownloadPath = string.Empty;

//    public static string LocalAssetBundleDownloadPath {
//        get {
//            if (!string.IsNullOrEmpty(assetBundleLocalDownloadPath))
//                assetBundleLocalDownloadPath = Application.streamingAssetsPath + "/AssetBundle/" +
//#if UNITY_EDITOR
//                GetPlatformFolderForAssetBundles(EditorUserBuildSettings.activeBuildTarget);
//#else
//			    GetPlatformFolderForAssetBundles(Application.platform);
//#endif
//            return assetBundleLocalDownloadPath;
//        }
//    }

    //public static string GetAssetBundleOutPath() {
    //    string outPath = getPlatformPath() + "/" + GetPlatformFolderForAssetBundles(Application.platform);
    //    if (!Directory.Exists(outPath)) {
    //        Directory.CreateDirectory(outPath);
    //    }
    //    return outPath;
    //}

    /// <summary>
    /// 获取平台路径
    /// </summary>
    /// <returns></returns>
    public static string getPlatformPath() {
        switch (Application.platform) {
            case RuntimePlatform.WindowsEditor:
                return Application.streamingAssetsPath;
            case RuntimePlatform.Android:
                return Application.persistentDataPath;
            case RuntimePlatform.IPhonePlayer:
                return Application.persistentDataPath;
            default:
                return null;

        }
    }

    /// <summary>
    /// 获取相对路径
    /// </summary>
    /// <returns></returns>
    public static string GetRelativePath() {
        if (Application.isEditor)
            return "file://" + System.Environment.CurrentDirectory.Replace("\\", "/"); // Use the build output folder directly.
#if !UNITY_2017_1_OR_NEWER
        else if (Application.isWebPlayer)
            return System.IO.Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/") + "/StreamingAssets";
#endif
        else if (Application.isMobilePlatform || Application.isConsolePlatform)
            return Application.streamingAssetsPath;
        else // For standalone player.
            return "file://" + Application.streamingAssetsPath;
    }

#if UNITY_EDITOR
    /// <summary>
    /// 在编辑器模式下为assetBundle 获取平台文件夹名称
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
	public static string GetPlatformFolderForAssetBundles(BuildTarget target) {
        switch (target) {
            case BuildTarget.Android:
                return "Android";
            case BuildTarget.iOS:
                return "iOS";
#if !UNITY_2017_1_OR_NEWER
            case BuildTarget.WebPlayer:
                return "WebPlayer";
#endif
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                return "Windows";
            case BuildTarget.StandaloneOSXIntel:
            case BuildTarget.StandaloneOSXIntel64:
            case BuildTarget.StandaloneOSXUniversal:
                return "OSX";
            // Add more build targets for your own.
            // If you add more targets, don't forget to add the same platforms to GetPlatformFolderForAssetBundles(RuntimePlatform) function.
            default:
                return null;
        }
    }
#endif


    /// <summary>
    /// 在运行模式下获取平台文件夹名称
    /// </summary>
    /// <param name="platform"></param>
    /// <returns></returns>
    public static string GetPlatformFolderForAssetBundles(RuntimePlatform platform) {
        switch (platform) {
            case RuntimePlatform.Android:
                return "Android";
            case RuntimePlatform.IPhonePlayer:
                return "iOS";
#if !UNITY_2017_1_OR_NEWER
            case RuntimePlatform.WindowsWebPlayer:
            case RuntimePlatform.OSXWebPlayer:
                return "WebPlayer";
#endif
            case RuntimePlatform.WindowsPlayer:
                return "Windows";
            case RuntimePlatform.OSXPlayer:
                return "OSX";
            // Add more build platform for your own.
            // If you add more platforms, don't forget to add the same targets to GetPlatformFolderForAssetBundles(BuildTarget) function.
            default:
                return null;
        }
    }
}

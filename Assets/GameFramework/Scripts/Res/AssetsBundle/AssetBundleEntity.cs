#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            AssetBundleEntity.cs 
/// 作者(Author):                 Syman
/// 创建时间(CreateTime):          2017/11/9 12:41:15
/// 修改者列表(Modifier):
/// 模块描述(Module description):
/// 更新时间(Update Time):
/// </summary>
#endregion
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetBundleEntity {

    public string Key;//用于打包时候选定这是一个唯一的key
    public string Name;//名称
    public string Tag;//标记
    public bool IsFolder;//是否文件夹
    public bool IsFirstData;//是否初始数据

    private List<string> m_PathList = new List<string>();
    /// <summary>
    /// 路径集合
    /// </summary>
    public List<string> PathList {
        get { return m_PathList; }
    }
}

#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            AssetBundleManager.cs 
/// 作者(Author):                 Syman
/// 创建时间(CreateTime):          2017/9/15 15:17:9
/// 修改者列表(Modifier):
/// 模块描述(Module description):
/// 更新时间(Update Time):
/// </summary>
#endregion
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using LitJson;
// 资源包的下载进度
public delegate void OnDownloadProgressUpdate(float progress, bool isComplete, string error);
// 资源包加载完成
public delegate void OnLoadComplete(bool isSuccess, string error);
// 资源包异步加载完成委托
public delegate void OnAsyncLoadAssetComplete(UnityEngine.Object asset, bool isSuccess);

public class AssetBundleInfo {

    public string name;
    public List<string> assets = new List<string>();

}

public class AssetBundleAdapter {



    private static AssetBundleManifest manifest = null;

    private static Dictionary<string, AssetBundle> abDic = new Dictionary<string, AssetBundle>();

    /// <summary>
    /// 用于存储资源名和包名的关系
    /// </summary>
    private static Dictionary<string, string> AssetInfoDic = new Dictionary<string, string>();

    public static AssetBundleManifest LoadManifest() {
        //获取manifest
        AssetBundle manifestBundle = AssetBundle.LoadFromFile(AssetbundleUtil.Path + "/AssetBundle/"+
                         AssetbundleUtil.AssetBundle_FileName);

        manifest = (AssetBundleManifest)manifestBundle.LoadAsset("AssetBundleManifest");

        //获取资源和资源包的对应关系
        string AssetBundleInfos = IOUtil.GetFileText(AssetbundleUtil.Path + "/AssetBundleInfo.txt");

        List<AssetBundleInfo> abInfoList = new List<AssetBundleInfo>();
        
        abInfoList = JsonMapper.ToObject<List<AssetBundleInfo>>(AssetBundleInfos);
        //存储资源信息
        foreach (var ABinfo in abInfoList) {
            foreach (var asset in ABinfo.assets) {
                AssetInfoDic.Add(asset, ABinfo.name);
            }
        }

        abInfoList.Clear();

        return manifest;
    }


    /// <summary>
    /// 在本地同步加载资源
    /// </summary>
    /// <param name="abPath">资源路径</param>
    /// <returns>资源包</returns>
    public static AssetBundle LoadAB(string abPath) {

        if (abDic.ContainsKey(abPath)) {
            return abDic[abPath];
        }
        //获取一次总的AssetBundleManifest文件
        if (manifest == null) {

            manifest = LoadManifest();

        }
        if (manifest != null) {
            //获取依赖文件列表
            string[] cubedepends = manifest.GetAllDependencies(abPath);
            List<string> dependences = new List<string>();
            if (cubedepends.Length > 0)
                for (int i = 0; i < cubedepends.Length; i++) {
                    dependences.Add(cubedepends[i]);
                    Debug.Log(cubedepends[i]);
                    LoadAB(cubedepends[i]);
                }
            AssetBundle LoadedAB = AssetBundle.LoadFromFile(AssetbundleUtil.Path + "/AssetBundle/" + abPath);
            //加载依赖的AssetBundle
            abDic[abPath] = LoadedAB;
            return abDic[abPath];
        }
        return abDic[abPath];
    }
    /// <summary>
    /// 异步在本地加载资源
    /// </summary>
    /// <param name="abPath">资源路径</param>
    /// <param name="onDownloadProgressUpdate">加载进度回调</param>
    /// <returns></returns>
    public static IEnumerator LoadABAsync(string abPath, OnDownloadProgressUpdate onDownloadProgressUpdate) {

        if (abDic.ContainsKey(abPath)) {
            onDownloadProgressUpdate(1, true, "");
            yield break;
        }
        //获取一次总的AssetBundleManifest文件
        if (manifest == null) {

            AssetBundle manifestBundle = AssetBundle.LoadFromFile(AssetbundleUtil.Path + "/AssetBundle/"
                + AssetbundleUtil.AssetBundle_FileName);
            manifest = (AssetBundleManifest)manifestBundle.LoadAsset("AssetBundleManifest");
        }
        if (manifest != null) {
            //获取依赖文件列表
            string[] cubedepends = manifest.GetAllDependencies(abPath);
            Debug.Log(cubedepends.Length);
            for (int i = 0; i < cubedepends.Length; i++) {
                Debug.Log(cubedepends[i]);
                LoadAB(cubedepends[i]);
            }
            AssetBundleCreateRequest abRequest = AssetBundle.LoadFromFileAsync(AssetbundleUtil.Path + "/AssetBundle/" + abPath);

            while (!abRequest.isDone) {
                onDownloadProgressUpdate(abRequest.progress, false, "");
                yield return abRequest;
            }

            //LoadedAssetBundle LoadedAB = new LoadedAssetBundle(AssetBundle.LoadFromFile(AssetBundleConfig.AssetBundle_AbsolutePath + abPath));
            AssetBundle LoadedAB = abRequest.assetBundle;
            //加载依赖的AssetBundle
            abDic[abPath] = LoadedAB;
            onDownloadProgressUpdate(abRequest.progress, true, "");

        }

    }

    public static void UnLoad(UnityEngine.Object asset) {
        Resources.UnloadAsset(asset);

    }

    public static void UnLoadBundle(string bundleName) {

        abDic[bundleName].Unload(false);

    }

    public static T[] LoadAssetWithSubAssets<T>(string asset)
        where T : UnityEngine.Object {
        string abPath = string.Empty;
        if (AssetInfoDic.TryGetValue(asset, out abPath)) {
            AssetBundle ab;
            if (!abDic.TryGetValue(abPath, out ab)) {
                ab = LoadAB(abPath);
            }
            if (ab != null)
                return abDic[abPath].LoadAssetWithSubAssets<T>(asset);
        }

        return default(T[]);

    }

    public static T LoadAsset<T>( string asset)
       where T : UnityEngine.Object {
        asset= asset.ToLower();
        string abPath = string.Empty;
        if (AssetInfoDic.TryGetValue(asset, out abPath)) {

            AssetBundle ab;
            if (!abDic.TryGetValue(abPath, out ab)) {
                ab = LoadAB(abPath);
            }
            if (ab != null)
                return abDic[abPath].LoadAsset<T>(asset);
        }


        return default(T);
    }


    /// <summary>
    /// （同步加载）从内存中加载
    /// </summary>
    public static void LoadAssetBundleFromMemory(string path) {
        AssetBundle ab = AssetBundle.LoadFromMemory(File.ReadAllBytes(path));
        //使用里面的资源
        GameObject pic = ab.LoadAsset<GameObject>("dds");
    }



    public delegate void dlg_OnAssetBundleDownLoadOver();

    public void DownLoadAssetFromServerWithDependencies(string AssetsHost, string RootAssetsName, string AssetName,
        string savePath, dlg_OnAssetBundleDownLoadOver OnDownloadOver = null) {
        //StartCoroutine(DownLoadAssetsFromServer(AssetsHost, RootAssetsName, AssetName, savePath, OnDownloadOver));
    }

    IEnumerator DownLoadAssetsFromServer(string AssetsHost, string RootAssetsName,
        string AssetName, string saveLocalPath, dlg_OnAssetBundleDownLoadOver OnDownloadOver = null) {


        AssetBundle assetBundle = null;
        AssetBundle LocalManifestAssetBundle = null;    //用于存储依赖关系的 AssetBundle  
        AssetBundleManifest assetBundleManifestServer = null;  //服务器 总的依赖关系      
        AssetBundleManifest assetBundleManifestLocal = null;   //本地 总的依赖关系

        if (RootAssetsName != "")    //总依赖项为空的时候去加载总依赖项  
        {
            UnityWebRequest Request = UnityWebRequest.GetAssetBundle(AssetsHost + RootAssetsName);
            yield return Request.Send();
            assetBundle = DownloadHandlerAssetBundle.GetContent(Request);
            assetBundleManifestServer = assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            Debug.Log("1");
        }

        //获取需要加载物体的所有依赖项  
        string[] AllDependencies = new string[0];
        if (assetBundleManifestServer != null) {
            //根据名称获取依赖项  
            AllDependencies = assetBundleManifestServer.GetAllDependencies(AssetName);
        }
        foreach (var item in AllDependencies) {
            Debug.Log(item);
        }

        //下载队列 并获取每个资源的Hash值  
        Dictionary<string, Hash128> dicDownloadInfos = new Dictionary<string, Hash128>();
        for (int i = AllDependencies.Length - 1; i >= 0; i--) {
            dicDownloadInfos.Add(AllDependencies[i], assetBundleManifestServer.GetAssetBundleHash(AllDependencies[i]));
        }
        dicDownloadInfos.Add(AssetName, assetBundleManifestServer.GetAssetBundleHash(AssetName));



        if (assetBundleManifestServer != null)   //依赖文件不为空的话下载依赖文件  
        {
            //Debug.Log("Hash:" + assetBundleManifestServer.GetHashCode());
            dicDownloadInfos.Add(RootAssetsName, new Hash128(0, 0, 0, 0));
        }
        foreach (var item in dicDownloadInfos) {
            Debug.Log(string.Format("name:{0},hash128:{1}", item.Key, item.Value));
        }

        //卸载掉,无法同时加载多个配置文件  
        assetBundle.Unload(true);

        Debug.Log(saveLocalPath);
        if (File.Exists(saveLocalPath + RootAssetsName)) {
            LocalManifestAssetBundle = AssetBundle.LoadFromFile(saveLocalPath + RootAssetsName);
            assetBundleManifestLocal = LocalManifestAssetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
        }

        foreach (var item in dicDownloadInfos) {
            Debug.Log(item.Key);
            if (!CheckLocalFileNeedUpdate(item.Key, item.Value, RootAssetsName, saveLocalPath, assetBundleManifestLocal)) {
                Debug.Log("无需下载:" + item.Key);
                continue;
            } else {
                DeleteFile(saveLocalPath + item.Key);
            }
            //直接加载所有的依赖项就好了  
            WWW wwwAsset = new WWW(AssetsHost + "/" + item.Key);
            //获取加载进度
            while (!wwwAsset.isDone) {
                Debug.Log(string.Format("下载 {0} : {1:N1}%", item.Key, (wwwAsset.progress * 100)));
                yield return new WaitForSeconds(0.2f);
            }
            Debug.Log(string.Format("下载 {0} : {1:N1}%", item.Key, (wwwAsset.progress * 100)));
            //保存到本地  
            SaveAsset2LocalFile(saveLocalPath, item.Key, wwwAsset.bytes, wwwAsset.bytes.Length);

        }
    }


    /// <summary>  
    /// 检测本地文件是否存在已经是否是最新  
    /// </summary>  
    /// <param name="AssetName"></param>  
    /// <param name="RootAssetsName"></param>  
    /// <param name="localPath"></param>  
    /// <param name="serverAssetManifestfest"></param>  
    /// <param name="CheckCount"></param>  
    /// <returns></returns>  
    bool CheckLocalFileNeedUpdate(string AssetName, Hash128 hash128Server, string RootAssetsName, string localPath, AssetBundleManifest assetBundleManifestLocal) {
        Hash128 hash128Local;
        bool isNeedUpdate = false;
        if (!File.Exists(localPath + AssetName)) {
            return true;   //本地不存在,则一定更新  
        }

        if (!File.Exists(localPath + RootAssetsName))   //当本地依赖信息不存在时,更新  
        {
            isNeedUpdate = true;
        } else   //总的依赖信息存在切文件已存在  对比本地和服务器两个文件的Hash值  
          {
            if (hash128Server == new Hash128(0, 0, 0, 0)) {
                return true;  //保证每次都下载总依赖文件  
            }
            hash128Local = assetBundleManifestLocal.GetAssetBundleHash(AssetName);
            //对比本地与服务器上的AssetBundleHash  版本不一致就下载  
            if (hash128Local != hash128Server) {
                isNeedUpdate = true;
            }
        }
        return isNeedUpdate;
    }

    /// <summary>  
    /// 删除文件  
    /// </summary>  
    /// <param name="path"></param>  
    void DeleteFile(string path) {
        if (!File.Exists(path)) {
            return;
        }
        File.Delete(path);
    }

    /// <summary>  
    /// 将文件模型创建到本地  
    /// </summary>  
    /// <param name="path"></param>  
    /// <param name="name"></param>  
    /// <param name="info"></param>  
    /// <param name="length"></param>  
    void SaveAsset2LocalFile(string path, string name, byte[] info, int length) {
        string wholePath = path + name;
        string dirPath = null;
        if (name.IndexOf('/') > -1) {
            dirPath = wholePath.Substring(0, wholePath.LastIndexOf('/'));
            Debug.Log(dirPath);
            if (!File.Exists(dirPath)) {
                Directory.CreateDirectory(dirPath);
            }
        }
        Stream sw = null;
        FileInfo fileInfo = new FileInfo(wholePath);
        if (fileInfo.Exists) {
            fileInfo.Delete();
        }
        //如果此文件不存在则创建  
        sw = fileInfo.Create();
        //写入  
        sw.Write(info, 0, length);
        sw.Flush();
        //关闭流  
        sw.Close();
        //销毁流  
        sw.Dispose();
        Debug.Log(name + "成功保存到本地~");
    }

}

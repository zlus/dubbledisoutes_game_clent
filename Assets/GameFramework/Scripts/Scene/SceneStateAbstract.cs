using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SceneStateAbstract {

    private string m_stateName = "ISceneState";
    public string StateName {
        get { return m_stateName; }
        set { m_stateName = value; }
    }
    //protected SceneStateController m_Controller = null;
    public SceneStateAbstract() {
        //m_Controller = Controller;
    }

    public virtual void StateBegin() { }
    public virtual void StateEnd() { }
    public virtual void StateLoadding(LoadType loadType, float progress) { }

    public override string ToString() {
        return string.Format("[I_SceneState:StateName={0}]", StateName);
    }
}

/*=================================================================
* Author: Zlu
* CreatedTime: 11/15/2017 10:33:38 AM
* Description: <功能描述> 
===================================================================*/

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public enum LoadType {
    RES=0,
    SCENE
}

public class SceneStateController : ServiceModule<SceneStateController>
{

    private SceneStateAbstract m_State;
    private string m_LoadSceneName = string.Empty;
    private bool m_bRunBegin = false;
    public SceneStateController() { }

    public void SetState(SceneStateAbstract State, string LoadSceneName) {
        if (string.IsNullOrEmpty(LoadSceneName)) return;
        if (!string.IsNullOrEmpty(m_LoadSceneName)) return;
        m_LoadSceneName = LoadSceneName;
        m_bRunBegin = false;
        LoadScene(LoadSceneName, State);
        if (m_State != null)
            m_State.StateEnd();

        m_State = State;
    }
    private void LoadScene(string LoadSceneName, SceneStateAbstract State) {
        if (LoadSceneName == null || LoadSceneName.Length == 0) {
            SceneManager.LoadScene("LoaddingScene", LoadSceneMode.Single);
        }
        // 1. 加载资源-------目前设计一个场景打一个包
        //  a. 加载声音
        //  b. 加载图片
        //  c. 。。。。
        // 2.加载场景
        //ResManager.Instance.LoadAssetBundleAsync(LoadSceneName, AssetBundleLoading);

        GameSystem.Instance.StartCoroutine(Loadding(State));

    }

    private void AssetBundleLoading(float progress, bool isComplete, string error) {

        m_State.StateLoadding(LoadType.SCENE, progress);
        //if (isComplete == true)
           // GameSystem.Instance.StartCoroutine(Loadding(State));
    }

    IEnumerator Loadding(SceneStateAbstract State) {

        int displayProgress = 0;
        int toProgress = 0;
        AsyncOperation op = SceneManager.LoadSceneAsync(m_LoadSceneName, LoadSceneMode.Single);
        op.allowSceneActivation = false;
        while(op.progress<0.9f) {
            toProgress = (int)op.progress * 100;
            while(displayProgress<toProgress) {
                ++displayProgress;
                State.StateLoadding(LoadType.SCENE, displayProgress);
                yield return new WaitForEndOfFrame();
            }
        }
        toProgress = 100;
        while(displayProgress<toProgress) {
            ++displayProgress;
            Debug.Log(State);
            State.StateLoadding(LoadType.SCENE, displayProgress);
            yield return new WaitForEndOfFrame();
        }
        op.allowSceneActivation = true;

        m_LoadSceneName = string.Empty;
        State.StateBegin();
    }

}

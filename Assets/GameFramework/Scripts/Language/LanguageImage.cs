using System.Collections;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Image))]
public class LanguageImage:LanguageComponentBase
{
    private Image image;
    public void Awake()
    {
        image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        Refresh();
        LanguageManager.Instance.languageChangeEvent += Refresh;
    }

    private void OnDisable()
    {
        LanguageManager.Instance.languageChangeEvent -= Refresh;
    }

    public void SetImage(string key = null)
    {
        if (string.IsNullOrEmpty(key)) image.sprite = null;
        this.Key = key;
        Value = LanguageManager.Instance.GetSpritePath(key, Value);
        image.sprite = ResManager.Instance.LoadAsset<Sprite>(Value);
    }

    public override string Tag
    {
        get
        {
            return "Image";
        }
    }

    public override void Refresh()
    {
        Value = LanguageManager.Instance.GetSpritePath(Key, Value);
        image.sprite = ResManager.Instance.LoadAsset<Sprite>(Value);
    }


}

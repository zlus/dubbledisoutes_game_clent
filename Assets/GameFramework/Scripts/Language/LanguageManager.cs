﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
using System;
using System.Collections;

public class LanguageManager :ServiceModule<LanguageManager>
{
    #region 属性字段
    //当前所选语种的文本数据字典集合
    private Dictionary<string, string> textLanguageDic = new Dictionary<string, string>();
    //当前所选语种的图片精灵路径数据字典集合
    private Dictionary<string, string> imagelanguageDic = new Dictionary<string, string>();

    public event Action languageChangeEvent;

    private string currentLanguage = Languages.ChineseSimplified;

    public string CurrentLanguage
    {
        get
        {
            return currentLanguage;
        }
        set
        {
            currentLanguage = value;
        }
    }

    public Dictionary<string, string> TextLanguageDic
    {
        get
        {
            return textLanguageDic;
        }
    }

    public Dictionary<string, string> ImagelanguageDic
    {
        get
        {
            return imagelanguageDic;
        }
    }
    #endregion

    #region 读取对应语言的json文件并将存储到字典中
    /// <summary>
    /// 读取对应语言的json文件并将存储到字典中
    /// </summary>
    /// <param name="language"></param>
    public void ReadAllLanguageJsonFileToDic()
    {
        ReadTextLanguageJsonFileToDic();
        ReadImageLanguageJsonFileToDic();
    }
    public void ReadTextLanguageJsonFileToDic()
    {

            TextAsset languageStaticTextAsset = Resources.Load<TextAsset>(LanguageDefine.LanguageJsonPathNoResources + currentLanguage + "_Text");
            textLanguageDic = JsonMapper.ToObject<Dictionary<string, string>>(languageStaticTextAsset.text);
    }
    public void ReadImageLanguageJsonFileToDic()
    {
        
            TextAsset languageDynamicTextAsset = Resources.Load<TextAsset>(LanguageDefine.LanguageJsonPathNoResources + currentLanguage + "_Image");
            imagelanguageDic = JsonMapper.ToObject<Dictionary<string, string>>(languageDynamicTextAsset.text);
        
    }
    #endregion

    #region 方法
    /// <summary>
    /// 更新语言
    /// </summary>
    public void UpdateLanguage()
    {

        if (languageChangeEvent != null)
        {
            languageChangeEvent();
            //languageStaticDic.Clear();
        }
    }
    #endregion

    #region 对外接口
    /// <summary>
    /// 通过键来获取文本内容
    /// </summary>
    /// <param name="key"></param>
    /// <param name="fallback">在找不到该键时返回的值</param>
    /// <returns></returns>
    public string GetTextContent(string key,string fallback="")
    {
        if (!textLanguageDic.ContainsKey(key))
        {
            this.LogWarning("Localization Key Not Found :" + key);
            return key;
        }
        return textLanguageDic[key];
    }
    /// <summary>
    /// 通过键来获取精灵路径
    /// </summary>
    /// <param name="key"></param>
    /// <param name="fallback">在找不到该键时返回的值</param>
    /// <returns></returns>
    public string GetSpritePath(string key, string fallback="")
    {
        if (!imagelanguageDic.ContainsKey(key))
        {
            this.LogWarning("Localization Key Not Found :" + key);
            return fallback;
        }
        
        return imagelanguageDic[key];
    }
    /// <summary>
    /// 设置语言
    /// </summary>
    /// <param name="language"></param>
    public void SetLanguage(string language)
    {
        if (currentLanguage == language) return;
        PlayerPrefs.SetString("DefaultLanguage", language);
        currentLanguage = language;
        RefreshLanguage();
    }

    /// <summary>
    /// 初始化(不可再Awake中初始化)
    /// </summary>
    public void Init()
    {
        //currentLanguage=Application.systemLanguage
        switch (Application.systemLanguage.ToString())
        {
            case Languages.English:
                currentLanguage = Languages.English;
                break;
            case Languages.ChineseSimplified:
                currentLanguage = Languages.ChineseSimplified;
                break;
            case Languages.ChineseTraditional:
                currentLanguage = Languages.ChineseTraditional;
                break;
            case "Chinese":
                currentLanguage = Languages.ChineseSimplified;
                break;
            default:
                currentLanguage = Languages.English;
                break;
        }
        if (PlayerPrefs.GetString("DefaultLanguage") != "")
        {
            currentLanguage = PlayerPrefs.GetString("DefaultLanguage");
        }
        ReadAllLanguageJsonFileToDic();
        UpdateLanguage();
    }

    /// <summary>
    /// 刷新语言
    /// </summary>
    public void RefreshLanguage()
    {

        ReadAllLanguageJsonFileToDic();
        UpdateLanguage();
        Debug.Log(currentLanguage);
    }
    #endregion
}

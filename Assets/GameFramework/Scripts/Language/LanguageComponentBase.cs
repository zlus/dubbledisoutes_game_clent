using UnityEngine;
public class LanguageComponentBase : MonoBehaviour
{
    [HideInInspector] public string Language;
    [HideInInspector] public string Module = "default";
    [HideInInspector] public string Key;
    [HideInInspector] public string Value;
    public virtual string Tag { get { return null; } }
    public virtual void Refresh()
    {

    } 
}
﻿using UnityEngine;
using System.Collections;

public static class LanguageDefine 
{

    public readonly static string LanguageExelPath = "/Exels/languageExel.xlsx";
    public readonly static string LanguageJsonPath = "/Resources/Files/Jsons/Languages/";
    public readonly static string LanguageJsonPathNoResources = "Files/Jsons/Languages/";

}
public static class Languages
{
    public const string English = "English";
    public const string ChineseSimplified = "ChineseSimplified";
    public const string ChineseTraditional = "ChineseTraditional";
}

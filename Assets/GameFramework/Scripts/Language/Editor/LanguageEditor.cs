using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using System.Linq;

[CustomEditor(typeof(LanguageComponentBase), true)]
public class LanguageEditor : Editor
{

    protected LanguageComponentBase m_target;
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();


        if (Application.isPlaying) return;
        m_target = target as LanguageComponentBase;
        //if (m_target.IsStaticText)
        //{
        FieldInfo[] Fields = typeof(Languages).GetFields(BindingFlags.Static | BindingFlags.Public);
        string[] languages = new string[Fields.Length];
        int number = 0;
        foreach (var temp in Fields)
        {

            languages[number] = temp.Name;

            number++;

        }
        //---------------------------------------语言列表设置----------------------------------------------//
        int languageIndex = Array.IndexOf(languages, LanguageManager.Instance.CurrentLanguage);
        int language = EditorGUILayout.Popup("Language", languageIndex, languages);
        if (language != languageIndex)
        {
            m_target.Language = languages[language];
            Debug.Log(m_target.Language);
            LanguageManager.Instance.CurrentLanguage = languages[language];



            //m_target.Refresh();
            EditorUtility.SetDirty(target);
        }
        LanguageManager.Instance.ReadAllLanguageJsonFileToDic();
        //------------------------------TextLanguageDic.Keys------------
        string[] keys;
        if (m_target.Tag=="Text")
        {
            keys = LanguageManager.Instance.TextLanguageDic.Keys.ToArray();
        }
        else
            keys = LanguageManager.Instance.ImagelanguageDic.Keys.ToArray();

        List<string> keysOfmodulePart = new List<string>();//模块列表
        foreach (var m_key in keys)
        {
            string[] temp = m_key.Split('.');
            if (!keysOfmodulePart.Contains(temp[0]))
            {
                keysOfmodulePart.Add(temp[0]);
            }
        }
        //---------------------------------------------------------//

        int moduleIndex = Array.IndexOf(keysOfmodulePart.ToArray(), m_target.Module);
        int i = EditorGUILayout.Popup("module", moduleIndex, keysOfmodulePart.ToArray());
        if (moduleIndex != i)
        {
            moduleIndex = i;
            m_target.Module = keysOfmodulePart[moduleIndex];
            EditorUtility.SetDirty(m_target);
        }
        List<string> keysnames = new List<string>();
        foreach (var m_key in keys)
        {
            string[] tep = m_key.Split('.');
            if (tep[0] == keysOfmodulePart[moduleIndex]) keysnames.Add(m_key);
        }
        int keyIndex = Array.IndexOf(keysnames.ToArray(), m_target.Key);

        int j = EditorGUILayout.Popup("Keys", keyIndex, keysnames.ToArray());

        if (j != keyIndex)
        {
            m_target.Key = keysnames[j];
            //m_target.Value = LanguageService.Instance().TextLanguageDic[keysnames[j]];

            if (m_target.Tag == "Text")
            {
                m_target.Value = LanguageManager.Instance.TextLanguageDic[keysnames[j]];
            }
            else
                m_target.Value = LanguageManager.Instance.ImagelanguageDic[keysnames[j]];
            EditorUtility.SetDirty(m_target);
        }
        if (!string.IsNullOrEmpty(m_target.Value))
        {
            EditorGUILayout.LabelField("Value", m_target.Value);
            m_target.Refresh();
        }
    }

}
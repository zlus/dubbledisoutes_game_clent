using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Excel;
using System.Data;
using LitJson;
using System.Text;
public class LanguageExcelToJson {

    private static string LOG_TAG = "LanguageExcelToJson";
    #region 编辑器扩展
    [MenuItem("localization/ExcelToJson")]
    static void ExelFileToJson()
    {
        //Debug.Log(Selection.GetFiltered();
        ExcelToJson(LanguageDefine.LanguageExelPath, LanguageDefine.LanguageJsonPath, 0);
        ExcelToJson(LanguageDefine.LanguageExelPath, LanguageDefine.LanguageJsonPath, 1);
    }
    //[MenuItem("localization/Languages/English")]
    //static void English()
    //{
    //    LanguageService.Instance().CurrentLanguage =Languages.English;
    //}
    //[MenuItem("localization/Languages/ChineseSimplified")]
    //static void ChineseSimplified()
    //{

    //}
    //[MenuItem("localization/Languages/ChineseTraditional")]
    //static void ChineseTraditional()
    //{

    //}

    /// <summary>
    /// 将多语言Excel配置文件转换成Json
    /// </summary>
    public static void ExcelToJson(string excelPath, string JsonPath, int TablesNumber)
    {
        
        Debuger.Log(LOG_TAG,Application.dataPath);
        FileStream stream = File.Open(Application.dataPath + excelPath, FileMode.Open, FileAccess.Read);
        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
        DataSet result = excelReader.AsDataSet();
        int columns = result.Tables[TablesNumber].Columns.Count;
        int rows = result.Tables[TablesNumber].Rows.Count;
        // int languageCount = columns - 1;//支持语言的数量

        Dictionary<string, string> ExcelDic = new Dictionary<string, string>();
        for (int i = 2; i < columns; i++)
        {
            ExcelDic.Clear();
            for (int j = 2; j < rows; j++)
            {

                ExcelDic.Add(result.Tables[TablesNumber].Rows[j][0].ToString(), result.Tables[TablesNumber].Rows[j][i].ToString());
                Debug.Log(result.Tables[TablesNumber].Rows[j][0].ToString() + result.Tables[TablesNumber].Rows[j][i].ToString());
            }
            JsonData jsonData = JsonMapper.ToJson(ExcelDic);
            string data = jsonData.ToString();

            FileInfo info = new FileInfo(Application.dataPath + JsonPath + result.Tables[TablesNumber].Rows[1][i].ToString() + ".txt");
            if (info != null) info.Delete();
            FileStream fs = new FileStream(Application.dataPath + JsonPath + result.Tables[TablesNumber].Rows[1][i].ToString() + ".txt", FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

            sw.Write(data);
            sw.Close();
            fs.Close();
        }

        AssetDatabase.Refresh();

    }
    #endregion
}

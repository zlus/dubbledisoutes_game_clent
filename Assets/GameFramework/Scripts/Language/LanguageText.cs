﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
[AddComponentMenu("Language/LanguageText")]
public class LanguageText : LanguageComponentBase
{
    private Text label;

    #region Unity回调
    public void Awake()
    {
        label = GetComponent<Text>();
    }

    private void OnEnable()
    {
        Refresh();
        LanguageManager.Instance.languageChangeEvent += Refresh;

    }

    private void OnDisable()
    {
        LanguageManager.Instance.languageChangeEvent -= Refresh;
    }
    #endregion 

    public override string Tag
    {
        get
        {
            return "Text";
        }
    }
    /// <summary>
    /// 动态设置文本文字，当不传值时清空文本
    /// </summary>
    /// <param name="key"></param>
    public void SetText(string key = null)
    {
        if (string.IsNullOrEmpty(key)) label.text = "";
        this.Key = key;
        label.text = LanguageManager.Instance.GetTextContent(key, label.text);
        this.Value = label.text;
    }
    public override void Refresh()
    {
#if UNITY_EDITOR
        label = GetComponent<Text>();
#endif

        label.text = LanguageManager.Instance.GetTextContent(Key, label.text);
    }

}

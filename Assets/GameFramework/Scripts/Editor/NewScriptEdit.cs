﻿#region 模块信息
/// <summary>
/// Copyright (C) 2017 
/// 文件名(File Name):            NewScriptEdit.cs 
/// 作者(Author):                 #AuthorName#
/// 创建时间(CreateTime):          #CreateTime#
/// 修改者列表(Modifier):
/// 模块描述(Module description):
/// 更新时间(Update Time):
/// </summary>
#endregion
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class NewScriptEdit : UnityEditor.AssetModificationProcessor
{

    private static void OnWillCreateAsset(string path)
    {
        path = path.Replace(".meta", "");
        if (path.EndsWith(".cs"))
        {
            string allText = File.ReadAllText(path);
            allText = allText.Replace("#AuthorName#", "Zlu")
                              .Replace("#CreateTime#", System.DateTime.Now.Year + "/" + System.DateTime.Now.Month
                + "/" + System.DateTime.Now.Day + " " + System.DateTime.Now.Hour + ":"
                + System.DateTime.Now.Minute + ":" + System.DateTime.Now.Second);

            File.WriteAllText(path, allText);
        }
    }
}

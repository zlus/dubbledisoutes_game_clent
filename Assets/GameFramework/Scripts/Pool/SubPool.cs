﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SubPool:IDisposable {
    Transform m_parent;

    //预设
    GameObject m_prefab;
    //集合
    List<GameObject> m_CurrentObjects = new List<GameObject>();
    Queue<GameObject> m_CacheObject = new Queue<GameObject>();
    /// <summary>
    /// 名字标识
    /// </summary>
    public string Name {
        get { return m_prefab.name; }
    }

    /// <summary>
    /// 构造
    /// </summary>
    public SubPool(Transform parent, GameObject prefab) {
        this.m_parent = parent;
        this.m_prefab = prefab;
    }

    /// <summary>
    /// 取对象
    /// </summary>
    public GameObject Spawn() {
        GameObject go = null;

        go = m_CacheObject.Dequeue();

        if (go == null) {
            go = GameObject.Instantiate<GameObject>(m_prefab);
            if(m_parent!=null)
            go.transform.parent = m_parent;
            m_CurrentObjects.Add(go);
        }

        go.SetActive(true);
        go.SendMessage("OnSpawn", SendMessageOptions.DontRequireReceiver);
        return go;
    }

    /// <summary>
    /// 回收活动对象
    /// </summary>
    public void Unspawn(GameObject go) {
        go.transform.SetParent(null);
        if (Contains(go)) {
            if(m_CacheObject.Count>=16) {
                
                return;
            }
            go.SendMessage("OnUnspawn", SendMessageOptions.DontRequireReceiver);
            go.SetActive(false);
            if (m_CurrentObjects.Contains(go))
                m_CurrentObjects.Remove(go);
            m_CacheObject.Enqueue(go);
        }
    }
    /// <summary>
    /// 清除指定物体
    /// </summary>
    /// <param name="obj"></param>
    public void ClearObj( GameObject obj ) {
        if(Contains(obj)) {
            m_CurrentObjects.Remove(obj);
            GameObject.Destroy(obj);
        }
    }

    /// <summary>
    /// 回收该池子的所有活动对象
    /// </summary>
    public void UnspawnAll() {
        foreach (GameObject item in m_CurrentObjects) {
                Unspawn(item);
        }
    }

    /// <summary>
    /// 清除缓存对象
    /// </summary>
    public void ClearCache() {
        while(m_CacheObject.Count>0) {
            GameObject.Destroy(m_CacheObject.Dequeue()); 
        }
    }

    /// <summary>
    /// 清除当前物体
    /// </summary>
    public void ClearCurrentObj() {
        for(int i=m_CurrentObjects.Count-1;i>=0;i--) {
            GameObject go = m_CurrentObjects[i];
            m_CurrentObjects.RemoveAt(i);
            GameObject.Destroy(go);
        }
    }

    /// <summary>
    /// 是否包含对象
    /// </summary>
    public bool Contains(GameObject go) {
        return m_CurrentObjects.Contains(go);
    }

    /// <summary>
    /// 清除缓存和当前物体
    /// </summary>
    public void Dispose() {
        ClearCurrentObj();
        ClearCache();
        m_CurrentObjects.Clear();
        m_CacheObject.Clear();
        m_prefab = null;
        m_parent = null;
    }
}
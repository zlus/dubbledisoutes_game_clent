﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ObjectPool : Singleton<ObjectPool>
{
    public string ResourceDir = "";

    Dictionary<string, SubPool> m_pools = new Dictionary<string, SubPool>();

    /// <summary>
    /// 取对象
    /// </summary>
    /// <param name="name"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public GameObject Spawn(string name,Transform parent=null)
    {
        if (!m_pools.ContainsKey(name))
            RegisterNew(name,parent);
        SubPool pool = m_pools[name];
        return pool.Spawn();
    }

    /// <summary>
    /// 回收对象
    /// </summary>
    /// <param name="go"></param>
    public void Unspawn(GameObject go)
    {
        SubPool pool = null;

        foreach (SubPool p in m_pools.Values)
        {
            if (p.Contains(go))
            {
                pool = p;
                break;
            }
        }

        pool.Unspawn(go);
    }

    /// <summary>
    /// 回收所有对象
    /// </summary>
    public void UnspawnAll() {
        foreach (SubPool p in m_pools.Values)
            p.UnspawnAll();
    }

    /// <summary>
    /// 回收当前子池子中的所有
    /// </summary>
    public void UnspawnSubPool(string name) {
        if (m_pools.ContainsKey(name))
            m_pools[name].UnspawnAll();
    }

    /// <summary>
    /// 清除对象
    /// </summary>
    public void ClearObj(GameObject go) {
        SubPool pool = null;

        foreach (SubPool p in m_pools.Values) {
            if (p.Contains(go)) {
                pool = p;
                break;
            }
        }

        pool.ClearObj(go);
    }



    /// <summary>
    /// 清除子池子中的缓存
    /// </summary>
    public void ClearSubPoolCache(string name) {
        if (m_pools.ContainsKey(name))
            m_pools[name].ClearCache();
    }
    /// <summary>
    ///  清除子池子中的所有场景中的物体
    /// </summary>
    public void ClearSubPoolCurrentObjs(string name) {
        if (m_pools.ContainsKey(name))
            m_pools[name].ClearCurrentObj();
    }

    /// <summary>
    /// 清除子池子
    /// </summary>
    /// <param name="name"></param>
    public void ClearSubPool(string name) {
        if (m_pools.ContainsKey(name))
            m_pools[name].Dispose();
        m_pools.Remove(name);
    }

    /// <summary>
    /// 创建新子池子
    /// </summary>
    /// <param name="name"></param>
    /// <param name="parent"></param>
    void RegisterNew(string name,Transform parent)
    {
        //预设路径
        string path = "";
        if (string.IsNullOrEmpty(ResourceDir.Trim()))
            path = name;
        else
            path = ResourceDir + "/" + name;

        //加载预设
        GameObject prefab =ResManager.Instance.LoadAsset<GameObject>(path);

        //创建子对象池
        SubPool pool = new SubPool(parent, prefab);
        m_pools.Add(pool.Name, pool);
    }
}
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;

public class ZoomButton : UIBehaviour,IPointerDownHandler,IPointerUpHandler {

    [SerializeField]
    private RectTransform zoomTarget;
    [SerializeField]
    private float zoomEndRange=0.9f;
    [SerializeField]
    private float tweenTime=0.2f;

    public Vector3 originScaleSize { get;private set; }

    public Vector3 currentScaleSize { get;private set; }

    public UnityEvent onClick;

    protected override void Awake()
    {
        originScaleSize = zoomTarget.localScale;
        currentScaleSize = originScaleSize;
    }

    public void SetcurrentScaleSize( bool hasTween,float zoomEndRange,float tweenTimet=0)
    {
        currentScaleSize *= zoomEndRange;
        if (hasTween)
        {
            zoomTarget.DOScale(currentScaleSize, tweenTime);
        }
        else
        {
            zoomTarget.DOScale(currentScaleSize, 0);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        zoomTarget.DOScale(currentScaleSize*zoomEndRange, tweenTime);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        zoomTarget.DOScale(currentScaleSize, tweenTime);
        if(onClick!=null)
        onClick.Invoke();
    }

}

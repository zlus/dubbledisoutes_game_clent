using DG.Tweening;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public enum ScrollDirection
{
    UP = 0,
    DOWN = 1

}
public class ScrollMovedEvent<T> : UnityEvent<T>
{

}
public class ScrollNavCompant : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{

    [SerializeField]
    private Scrollbar m_Scrollbar;
    [SerializeField]
    private int segments;
    [SerializeField]
    private RectTransform content;

    public ScrollMovedEvent<int> OnMoved=new ScrollMovedEvent<int>();
    private ScrollRect m_ScrollRect;

    private float VerticalNearestSegments;

    private float mTargetValue;

    public ScrollDirection scrollDirection { get; private set; }

    public ScrollRect ScrollRect
    {
        get
        {
            return m_ScrollRect;
        }
    }
    /// <summary>
    /// 是否需要吸附
    /// </summary>

    private Vector3 startPoint = new Vector3(0, 0, 0);

    private Vector3 endPoint = new Vector3(0, 0, 0);


    private void Awake()
    {
        RectTransform canvas = UIRoot.M_Canvas.GetComponent<RectTransform>();
        float rate = canvas.sizeDelta.y / 400.0f;
        m_ScrollRect = this.GetComponent<ScrollRect>();
        content.sizeDelta = new Vector2(canvas.sizeDelta.x/ rate, 400 * 3);
    }

    void Update()
    {
        if (m_ScrollRect.inertia)
        {
            m_Scrollbar.value = m_ScrollRect.verticalNormalizedPosition;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startPoint = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        endPoint = Input.mousePosition;
        float moveToValue = 0;
        int currentSegments= GetNearestSegments(m_ScrollRect.normalizedPosition.y, out moveToValue);
        m_ScrollRect.DOVerticalNormalizedPos(moveToValue, 0.3f);
        if(OnMoved != null)
            OnMoved.Invoke(currentSegments);
    }
    /// <summary>
    /// 获取最近的段位
    /// </summary>
    /// <param name="CurrPoint"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public int GetNearestSegments(float CurrPoint,out float value)
    {
        float segmentsDis = 1.0f / (segments-1);//获取段之间的距离
        float min_d_Value = 1;
        int NearestSegments = 0;

        //获取最近的段位
        for (int i = 0; i < segments; i++)
        {
            float d_Value =Mathf.Abs(CurrPoint - i * segmentsDis);//计算差值
            if (d_Value < min_d_Value)
            {
                min_d_Value = d_Value;
                NearestSegments = i;
            }
        }
        value = NearestSegments * segmentsDis;
        return NearestSegments;
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameConfig
{

    public const float BombRange = 35f;//炸弹碰撞器大小
    public const float PlayerRange = 26.5f;//玩家碰撞器大小
    public const float foodRange = 6;//食物碰撞器大小
    public const float dubbleRange = 6;//泡泡碰撞器大小
    public const float GameFoodAmount = 50;//场上食物的最大数量

}

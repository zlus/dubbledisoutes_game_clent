using UnityEngine;
using System.Collections;

public enum GameMode
{
    EndlessPVE,
    TimelimitPVE,
    EndlessPVP,
    TimelimitPVP
}

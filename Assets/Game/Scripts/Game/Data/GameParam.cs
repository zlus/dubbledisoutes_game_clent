﻿using UnityEngine;
using System.Collections;
using ProtoBuf;

[ProtoContract]
public class GameParam 
{
    /// <summary>
    /// 游戏ID，服务器上可能有多个游戏同时在进行
    /// </summary>
    [ProtoMember(1)] public int id = 0;
    /// <summary>
    /// 地图数据
    /// </summary>
    [ProtoMember(2)] public MapData mapData = new MapData();
    /// <summary>
    /// 随机数种子
    /// </summary>
    [ProtoMember(3)] public int randSeed = 0;

    /// <summary>
    /// 游戏模式
    /// </summary>
    [ProtoMember(4)] public GameMode mode = GameMode.EndlessPVE;

    /// <summary>
    /// 限时模式的时间
    /// </summary>
    [ProtoMember(5)] public int limitedTime=180;
}

using UnityEngine;
using System.Collections;
using ProtoBuf;
[ProtoContract]
public class MapData {

    /// <summary>
    /// 地图ID
    /// </summary>
    [ProtoMember(1)] public int id = 0;

    /// <summary>
    /// 地图的名字，用于在UI中显示
    /// </summary>
    [ProtoMember(2)] public string name = "";

}

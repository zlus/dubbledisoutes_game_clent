using UnityEngine;
using System.Collections;
using ProtoBuf;

[ProtoContract]
public class PlayerData
{

    /// <summary>
    /// 单局游戏中分配的id
    /// </summary>
    [ProtoMember(1)] public uint id;
    /// <summary>
    /// 玩家对应的用户ID
    /// </summary>
    [ProtoMember(2)] public uint userId;
    /// <summary>
    /// 玩家的名字
    /// </summary>
    [ProtoMember(3)] public string name;

    [ProtoMember(4)] public PlayerBubbleData bubbleData = new PlayerBubbleData();
    /// <summary>
    /// 玩家组队ID，预留字段，单人　id=0
    /// </summary>
    [ProtoMember(5)] public int teamId = 0;
    /// <summary>
    /// 玩家在单局中的得分
    /// </summary>
    [ProtoMember(6)] public int score = 0;
    /// <summary>
    /// 玩家排名
    /// </summary>
    [ProtoMember(7)] public int rankingIndex=0;

    /// <summary>
    /// AI 的值  如果是0则使用AI
    /// </summary>
    [ProtoMember(8)] public int ai = 0;

    /// <summary>
    /// 玩家的能量值
    /// </summary>
    [ProtoMember(9)] public int energy = 20;

    /// <summary>
    /// 玩家的行动力
    /// </summary>
    [ProtoMember(10)] public int mobility = 10;

    [ProtoMember(11)] public int energyStore = 20;  
    public int Mobility
    {
        get
        {
           // int mobility;
            return Mathf.Clamp((int)(energy * 0.5), 5, 35);
        }
    }


}

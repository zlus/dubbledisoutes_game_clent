using UnityEngine;

public class GameContext
{
    /// <summary>
    /// 游戏的启动参数
    /// </summary>
    public GameParam param = null;
    /// <summary>
    ///  随机数生成器
    /// </summary>
    public GameRandom random = new GameRandom();
    /// <summary>
    /// 当前是第几帧
    /// </summary>
    public int currentFrameIndex = 0;
    /// <summary>
    /// 地图大小
    /// </summary>
    public Low_Vector3 mapSize = new Low_Vector3();

    private DictionaryEx<int, Color> m_mapColor = new DictionaryEx<int, Color>();

    public Color GetUniqueColor(int colorId) {
        if (m_mapColor.ContainsKey(colorId))
        {
            return m_mapColor[colorId];
        }
        Color c = new Color(random.Rnd(), random.Rnd(), random.Rnd());
        m_mapColor.Add(colorId, c);
        return c;
    }

    public int getRande(int start,int end)
    {
        return random.Range(start, end);

    }

    public Low_Vector3 EntityToViewPoint(Low_Vector3 pos)
    {
        pos = pos - (mapSize / 2);
        pos.Z = 0;
        return pos;
    }
}

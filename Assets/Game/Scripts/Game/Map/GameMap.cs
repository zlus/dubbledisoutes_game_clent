using UnityEngine;
using System.Collections;

public class GameMap
{

    private MapScript m_script;
    private GameObject m_view;
    private Vector3 m_size;

    public Vector3 Size { get { return m_size; } }
    public GameObject View { get { return m_view; } }

    public void Load(MapData data)
    {
        //GameObject mapPrefab=new GameObject();//TODO 资源加载=Resources.Load<GameObject>("map/map_" + data.id);
        GameObject mapPrefab = ResManager.Instance.LoadGameObjectAsset(AppConfig.rootPath + "Prefabs/Map/map_" + data.id);
        m_view = GameObject.Instantiate(mapPrefab);

        Vector3 size = m_view.GetComponent<SpriteRenderer>().sprite.bounds.size;
        //计算缩放后的真正尺寸
        size.x *= m_view.transform.localScale.x;
        size.y *= m_view.transform.localScale.y;
        size.z *= m_view.transform.localScale.z;
        m_size = size;

        m_script = m_view.GetComponent<MapScript>();
        
    }

    public void UnLoad() {
        m_script = null;
        if (m_view != null)
        {
            GameObject.Destroy(m_view);
            m_view = null;
        }
    }
    public void Start()
    {
        m_script.Create();
    }

    public void EnterFrame(int frameIndex)
    {
        if (m_script != null)
        {
            m_script.EnterFrame(frameIndex);
        }
    }



}

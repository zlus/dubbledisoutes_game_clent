using UnityEngine;
using System.Collections;
using System;

public class MapScript : MonoBehaviour
{
    [SerializeField]
    private Vector3 m_size;

    private int m_lastActionFrame = 0;

    private GameContext m_context;

    public void Create()
    {
        m_size = GameManager.Instance.Context.mapSize;
        m_context = GameManager.Instance.Context;

        for(int i = 0; i < GameConfig.GameFoodAmount; i++)
        {
            GameManager.Instance.AddFoodRandom();
        }
        
    }
    public void EnterFrame(int frameIndex) {

        ///TODO 地图相关操作
        //每隔5秒钟
        float dt = frameIndex - m_lastActionFrame;
        if (dt > 150)
        {
            m_lastActionFrame = frameIndex;
            if (GameManager.Instance.GetFoodList().Count < GameConfig.GameFoodAmount)
            {
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
                GameManager.Instance.AddFoodRandom();
            }
            if (GameManager.Instance.GetPlayerList().Count < 5)
            {
                PlayerData data = new PlayerData();
                data.id = (uint)m_context.random.Range(100, 100000);
                data.bubbleData.id = m_context.random.Range(0,5);
                data.ai = 1;
                GameManager.Instance.RegPlayerData(data);
                GameManager.Instance.CreatePlayer(data.id);
            }
        }
    }


}

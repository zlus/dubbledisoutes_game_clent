﻿using UnityEngine;
using SGF.Network.FSPLite;
using System.Collections.Generic;

public class GameManager : ServiceModule<GameManager>
{
    private bool m_isRunning;
    private GameContext m_context;
    private GameMap m_map;
    public uint mainId;

    public int mainSocre=20;
    //玩家管理,食物管理
    private List<NormalFood> m_listFood = new List<NormalFood>();
    private List<Player> m_listPlayer = new List<Player>();
    private List<NormalBomb> m_listBomb = new List<NormalBomb>();
    private List<NormalDubble> m_listDubble = new List<NormalDubble>();
    
    
    private DictionaryEx<uint, PlayerData> m_mapPlayerData = new DictionaryEx<uint, PlayerData>();
    public event PlayerDieEvent onPlayerDie;

    public bool IsRunning { get { return m_isRunning; } }
    public GameContext Context { get { return m_context; } }
    public GameMode GameMode { get { return m_context.param.mode; } }


    public void CreateGame(GameParam param) {

        if (m_isRunning)
        {
            this.LogError("Create() Game Is Runing Already!");
            return;
        }
        this.Log("Create() param:{0}", param);
        //创建上下文，保存战斗全局参数
        m_context = new GameContext();
        m_context.param = param;
        m_context.random.Seed = param.randSeed;
        m_context.currentFrameIndex = 0;

        //创建地图
        m_map = new GameMap();
        m_map.Load(param.mapData);
        m_context.mapSize = m_map.Size;

        //TODO 其他初始化操作
        EntityFactory.Init();
        ViewFactory.Init(m_map.View.transform);

        m_map.Start();

        GameCamera.Create();
        m_isRunning = true;
    }

    public int Score;

    public void ReleaseGame() {
       // Score = m_mapPlayerData[mainId].energyStore;
        if (!m_isRunning) {
            return;
        }
        //TODO 其他初始化操作游戏释放操作
        m_isRunning = false;
        GameCamera.Release();

        m_listFood.Clear();
        m_listPlayer.Clear();
        m_listBomb.Clear();
        m_listDubble.Clear();

        ViewFactory.Release();
        EntityFactory.Release();

        if (m_map != null)
        {
            m_map.UnLoad();
            m_map = null;
        }

        onPlayerDie = null;
    }

    public void InputVKey(int vkey, float arg, uint playerId) {

        if(playerId == 0)
        {
            HandleOtherVkey(vkey, arg, playerId);
        }
        else
        {
            Player player = GetPlayer(playerId);
            if (player != null)
            {
                player.InputVKey(vkey, arg);
            }
            else
            {
                HandleOtherVkey(vkey, arg, playerId);
            }
        }

    }

    private void HandleOtherVkey(int vkey, float arg, uint playerId) {

        bool hasHandled = false;
        //短路机制
        hasHandled = hasHandled || DoVKey_CreatePlayer(vkey, arg, playerId);
        hasHandled = hasHandled || DoVKey_ReleasePlayer(vkey, arg, playerId);

    }

    private bool DoVKey_CreatePlayer(int vkey, float arg, uint playerId) {
        if (vkey == GameVKey.CreatePlayer)
        {
            CreatePlayer(playerId);
            return true;
        }
        return false;
    }

    private bool DoVKey_ReleasePlayer(int vkey,float arg,uint playerId)
    {
        if (vkey == FSPVKeyBase.GAME_EXIT) {

            ReleasePlayer(playerId);
            return true;

        }

        return false;
    }
    //==============================================================================
    public void EnterFrame(int frameIndex) {


        if (!m_isRunning)
        {
            return;
        }

        if (frameIndex < 0) {
            m_context.currentFrameIndex++;
        }
        else
        {
            m_context.currentFrameIndex = frameIndex;
        }
        EntityFactory.ClearReleasedObjects();

        for(int i = 0; i < m_listPlayer.Count; ++i)
        {
            m_listPlayer[i].EnterFrame(frameIndex);
        }

        for(int i = 0; i < m_listDubble.Count; ++i)
        {
            m_listDubble[i].EnterFrame(frameIndex);
        }

        for(int i = 0; i < m_listBomb.Count; ++i)
        {
            m_listBomb[i].EnterFrame(frameIndex);
        }
        List<uint> listDiePlayerId = new List<uint>();
        for (int i = 0; i < m_listPlayer.Count; i++)
        {

            Player player = m_listPlayer[i];


            Player beHitPlayer = player.TryHitEnemies(m_listPlayer);

            if (beHitPlayer != null)
            {
                if(beHitPlayer.Data.id == mainId)
                {
                    mainSocre = beHitPlayer.Data.energyStore;
                }
                listDiePlayerId.Add(beHitPlayer.Data.id);
                ReleasePlayer(beHitPlayer.Data.id);
                
                i--;
                continue;
            }
           
            
            for(int j = 0; j < m_listFood.Count; j++)
            {
                NormalFood food = m_listFood[j];
                if (player.TryEatFood(food))
                {
                    RemoveFoodAt(j);
                    j--;
                }

            }

            for(int k = 0; k < m_listDubble.Count; k++)
            {
                NormalDubble dubble = m_listDubble[k];
                if (player.IsHitDubble(dubble))
                {
                    RemoveDubbleAt(k);
                    k--;
                }
            }
            for(int l = 0; l < m_listBomb.Count; l++)
            {
                NormalBomb bomb = m_listBomb[l];
                player.IsHitBomb(bomb);
            }

        }



        if (m_map != null)
        {
            m_map.EnterFrame(frameIndex);
        }

        //响应玩家死亡
        if (onPlayerDie != null)
        {
            for (int i = 0; i < listDiePlayerId.Count; ++i)
            {
                onPlayerDie(listDiePlayerId[i]);
            }
        }

    }
    //========================================================================



    public void RegPlayerData(PlayerData data)
    {
        m_mapPlayerData[data.id] = data;
    }

    /// <summary>
    /// 创建角色
    /// </summary>
    /// <param name="playerId"></param>
    public void CreatePlayer(uint playerId)
    {
        PlayerData data = m_mapPlayerData[playerId];
        data.energy = 20;
        data.energyStore = 20;
        Vector3 mapSize = m_context.mapSize;
        Vector3 posMax = mapSize * 0.7f;
        Vector3 posMin = mapSize - posMax;
        Low_Vector3 pos = new Low_Vector3();
        pos.X = m_context.random.Range(posMin.x, posMax.x);
        pos.Y = m_context.random.Range(posMin.y, posMax.y);
        pos.Z = m_context.random.Range(posMin.z, posMax.z);
        pos.Z = 0;
        Player player= EntityFactory.InstanceEntity<Player>();
        player.Create(data, pos);
        m_listPlayer.Add(player);

    }

    /// <summary>
    /// 释放角色
    /// </summary>
    /// <param name="playerId"></param>
    private void ReleasePlayer(uint playerId)
    {
        int index = GetPlayerIndex(playerId);
        ReleasePlayerAt(index);
    }

    private void ReleasePlayerAt(int index) {
        if (index >= 0)
        {
            Player player = m_listPlayer[index];
            m_listPlayer.RemoveAt(index);

            player.ReleaseInFactory();
        }
    }
    /// <summary>
    /// 通过角色ID　获取角色
    /// </summary>
    /// <param name="playerId"></param>
    /// <returns></returns>
    public Player GetPlayer(uint playerId)
    {
        int index = GetPlayerIndex(playerId);
        if (index >= 0)
        {
            return m_listPlayer[index];
        }
        return null;
    }
    private int GetPlayerIndex(uint playerId)
    {
        for (int i = 0; i < m_listPlayer.Count; i++)
        {
            if (m_listPlayer[i].Id == playerId)
            {
                return i;
            }
        }
        return -1;
    }
    public List<Player> GetPlayerList()
    {
        return m_listPlayer;
    }

    //=====================================================================

    public void AddFoodRandom(Low_Vector3 position,Low_Float range,int count)
    {
        for(int i = 0; i < count; i++)
        {
            Low_Vector3 pos = new Low_Vector3();
            pos.X = m_context.random.Range(position.X - range, position.X + range);
            pos.Y = m_context.random.Range(position.Y - range, position.Y + range);
            pos.Z = 0;
            int type = m_context.random.Range(0, 5);
            AddFood(pos, type);
        }

    }

    public void AddFoodRandom()
    {
        Low_Vector3 pos=new Low_Vector3();
        pos.X = m_context.random.Range(0, m_context.mapSize.X);
        pos.Y = m_context.random.Range(0, m_context.mapSize.Y);
        pos.Z = m_context.random.Range(0, m_context.mapSize.Z);

        pos.Z = 0;
        int type = m_context.random.Range(0, 5);
        AddFood(pos, type);
    }

    public void AddFood(Low_Vector3 pos,int type)
    {
        NormalFood food = EntityFactory.InstanceEntity<NormalFood>();
        food.Create(type, pos);
        m_listFood.Add(food);
    }

    public void RemoveFoodAt(int i)
    {
        NormalFood food = m_listFood[i];
        m_listFood.RemoveAt(i);
        EntityFactory.ReleaseEntity(food);
    }
    public List<NormalFood> GetFoodList()
    {
        return m_listFood;
    }

    //======================================================================
    public void AddBomb(int type,Low_Vector3 startPos,Low_Vector3 angle)
    {
        NormalBomb bomb = EntityFactory.InstanceEntity<NormalBomb>();
        bomb.Create(type, startPos, angle);
        m_listBomb.Add(bomb);
    }

    public void RemoveBomb(NormalBomb bomb)
    {
        EntityFactory.ReleaseEntity(bomb);
        m_listBomb.Remove(bomb);
    }

    public void RemoveBombAt(int i)
    {
        NormalBomb bomb = m_listBomb[i];
        m_listBomb.RemoveAt(i);
        EntityFactory.ReleaseEntity(bomb);
    }

    public List<NormalBomb> GetBombList()
    {
        return m_listBomb;
    }

    //=======================================================================

    public void AddDubble(int type, uint playerId, Low_Vector3 startPos, Low_Vector3 angle)
    {
        NormalDubble dubble= EntityFactory.InstanceEntity<NormalDubble>();
        dubble.Create(type, playerId, startPos, angle);
        m_listDubble.Add(dubble);
    }

    public void RemoveBubble(NormalDubble entity)
    {
        m_listDubble.Remove(entity);
        EntityFactory.ReleaseEntity(entity);
    }

    public void RemoveDubbleAt(int i)
    {
        NormalDubble dubble = m_listDubble[i];
        m_listDubble.RemoveAt(i);
        EntityFactory.ReleaseEntity(dubble);
    }

    public List<NormalDubble> GetDubble()
    {
        return m_listDubble;
    }

}

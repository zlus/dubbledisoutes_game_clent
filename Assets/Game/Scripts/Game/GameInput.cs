using UnityEngine;
using System.Collections;
using System;
public class GameInput : MonoBehaviour
{
    private static DictionaryEx<KeyCode, bool> m_MapKeyState = new DictionaryEx<KeyCode, bool>();
    public static Action<int, float> OnVkey;
    private static GameInput m_Instance = null;
    [SerializeField]
    private EasyJoystick m_JoysTick;
    [SerializeField]
    private EasyButton[] m_buttons;

    public int ButtonCount { get;private set; }
    public static void Create()
    {
        if (m_Instance != null)
        {
            return;
        }
        GameObject prefab = ResManager.Instance.LoadGameObjectAsset(AppConfig.rootPath+"Prefabs/GameInput");
        GameObject go = GameObject.Instantiate(prefab);
        m_Instance = GameObjectUtils.EnsureComponent<GameInput>(go);
    }

    public static void Release() {
        m_MapKeyState.Clear();
        if (m_Instance != null)
        {
            GameObject.Destroy(m_Instance.gameObject);
            m_Instance = null;
        }
        OnVkey = null;
    }


    void OnEnable()
    {
        ButtonCount = m_buttons.Length;
        EasyJoystick.On_JoystickMove += On_JoystickMove;
        EasyJoystick.On_JoystickMoveEnd += On_JoystickMoveEnd;
        EasyButton.On_ButtonUp += On_ButtonUp;
        EasyButton.On_ButtonDown += On_ButtonDown;
    }

    void OnDisable()
    {
        EasyJoystick.On_JoystickMove -= On_JoystickMove;
        EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
        EasyButton.On_ButtonDown -= On_ButtonDown;
        EasyButton.On_ButtonUp -= On_ButtonUp;
    }

    void OnDestroy()
    {
        EasyJoystick.On_JoystickMove -= On_JoystickMove;
        EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
        EasyButton.On_ButtonDown -= On_ButtonDown;
        EasyButton.On_ButtonUp -= On_ButtonUp;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)){
            On_ButtonDown("speedUp");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            On_ButtonDown("bomb");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            On_ButtonDown("dubble");
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            On_ButtonUp("speedUp");
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            On_ButtonUp("bomb");
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            On_ButtonUp("dubble");
        }

    }

    private void On_ButtonDown(string buttonName)
    {
        Debug.Log(buttonName);
        for(int i = 0; i < ButtonCount; i++)
        {
            if (buttonName == m_buttons[i].name)
            {
                HandleVKey(i+1, 1);
                break;
            }
        }
    }

    private void On_ButtonUp(string buttonName)
    {
        for (int i = 0; i < ButtonCount; i++)
        {
            if (buttonName == m_buttons[i].name)
            {
                HandleVKey(i + 1, 0);
                break;
            }
        }
    }

    private void On_JoystickMoveEnd(MovingJoystick move)
    {
        if (move.joystick == m_JoysTick)
        {

            HandleVKey(GameVKey.MoveX, 0);
            HandleVKey(GameVKey.MoveY, 0);
        }
    }

    private void On_JoystickMove(MovingJoystick move)
    {
        if (move.joystick == m_JoysTick)
        {
            HandleVKey(GameVKey.MoveX, move.joystickValue.x);
            HandleVKey(GameVKey.MoveY, move.joystickValue.y);
        }
    }
    private void HandleVKey(int vkey, float arg)
    {
        if (OnVkey != null)
        {
            OnVkey(vkey, arg);
        }
    }
}

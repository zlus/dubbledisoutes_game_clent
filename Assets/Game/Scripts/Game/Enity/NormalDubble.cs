﻿using UnityEngine;
using System.Collections;
using System;

public class NormalDubble : EntityObject
{
    private int m_type = 0;
    private Low_Vector3 startPos;
    private Low_Vector3 Pos;
    private Low_Float radians;
    public Low_Float range = GameConfig.dubbleRange;
    private int timer = 0;
    private int lifeTime = 180;
    public uint PlayerID { get;private set; }
    public bool IsBoomDuring { get; private set; }
    public Low_Circle Collider
    {
        get
        {
            return new Low_Circle(Pos, range);
        }
        private set
        {
        }
    }
    public void Create(int type, uint playerId, Low_Vector3 startPos, Low_Vector3 angle)
    {
        m_type = type;
        this.startPos = startPos;
        Pos = startPos;
        PlayerID = playerId;
        radians = Low_Math.DegreesToRadians(angle.Z);
        ViewFactory.CreateView(AppConfig.rootPath+ "Prefabs/Dubble/dubble" + m_type, AppConfig.rootPath + "Prefabs/Dubble/dubble_0", this);
    }
    
    public void EnterFrame(int frameIndex)
    {
        timer++;
        Pos = new Low_Vector2(startPos.X - 6*timer * Low_Math.Sin(radians), startPos.Y + 6*timer * Low_Math.Cos(radians));
        if (timer >= lifeTime)
        {
            //TODO 释放自身
            GameManager.Instance.RemoveBubble(this);
        }
    }

    public override Vector3 Position()
    {
        return Pos;
    }

    protected override void Release()
    {
        timer = 0;
        ViewFactory.ReleaseView(this);
    }

}

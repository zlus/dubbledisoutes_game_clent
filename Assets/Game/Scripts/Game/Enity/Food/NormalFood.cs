﻿using UnityEngine;
using System.Collections;
using System;

public class NormalFood : EntityObject
{
    private int m_type = 0;
    private Vector3 m_pos;
    public  Low_Circle Collider { get; private set; }
    public void Create(int type, Vector3 pos)
    {
        m_type = type;
        m_pos = pos;
        Collider = new Low_Circle(pos, GameConfig.foodRange);
        ViewFactory.CreateView(AppConfig.rootPath+"Prefabs/Food/food_" + m_type, AppConfig.rootPath+"Prefabs/Food/food_0", this);
    }
    protected override void Release()
    {
        ViewFactory.ReleaseView(this);
    }

    public override Vector3 Position()
    {
        return m_pos;
    }
}

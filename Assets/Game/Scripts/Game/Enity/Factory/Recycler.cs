﻿using System.Collections.Generic;
public interface IRecycleObject
{
    string GetRecycleType();
    void Dispose();
}
 public class Recycler
{
    private static DictionaryEx<string, Stack<IRecycleObject>> m_poolIdleObject;

    public Recycler()
    {
        m_poolIdleObject = new DictionaryEx<string, Stack<IRecycleObject>>();
    }

    public void Release()
    {
        foreach(var pair in m_poolIdleObject)
        {
            foreach(var obj in pair.Value)
            {
                obj.Dispose();
            }
            pair.Value.Clear();
        }
    }

    public void Push(IRecycleObject obj)
    {
        string type = obj.GetRecycleType();
        Stack<IRecycleObject> stackIdleObject = m_poolIdleObject[type];
        if (stackIdleObject == null)
        {
            stackIdleObject = new Stack<IRecycleObject>();
            m_poolIdleObject.Add(type, stackIdleObject);
        }
        stackIdleObject.Push(obj);
    }

    public IRecycleObject Pop(string type)
    {
        Stack<IRecycleObject> stackIdleObject = m_poolIdleObject[type];
        if (stackIdleObject != null && stackIdleObject.Count > 0)
        {
            return stackIdleObject.Pop();
        }
        return null;
    }
}

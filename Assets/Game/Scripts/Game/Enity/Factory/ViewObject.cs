﻿using UnityEngine;
public abstract class ViewObject :MonoBehaviour,IRecycleObject
{

    private string m_recycleType;

    internal void CreateInFactory(EntityObject entity,string recycleType)
    {
        m_recycleType = recycleType;
        Create(entity);
    }

    protected abstract void Create(EntityObject entity);

    internal void ReleaseInFactory()
    {
        Release();
    }
    protected abstract void Release();
    public string GetRecycleType()
    {
        return m_recycleType;
    }
    public void Dispose()
    {
        try
        {
            GameObject.Destroy(this.gameObject);
        }
        catch (System.Exception e)
        {

        }
    }
}
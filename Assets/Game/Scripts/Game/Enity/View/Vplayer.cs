﻿using UnityEngine;
using System.Collections;
using System;

public class Vplayer : ViewObject
{
    [SerializeField]
    private Vector3 m_entityPosition;
    [SerializeField]
    private Transform Pointer;
    [SerializeField]
    private SpriteRenderer IceObj;
    private Player m_entity;

    private GameContext m_context;

    [SerializeField][Header("能量")]
    private int energy;
    [SerializeField][Header("行动力")]
    private int mobility;
    protected override void Create(EntityObject entity)
    {
        m_entity = entity as Player;
        m_context = GameManager.Instance.Context;
        GameObject prefab= ResManager.Instance.LoadGameObjectAsset(AppConfig.rootPath+"Prefabs/Player/Player_" + m_entity.bubbleData.id);
        GameObject go = Instantiate(prefab);
        go.transform.SetParent(this.transform);
        go.transform.localPosition = new Vector3(0, 0, 0);
        this.transform.position = m_context.EntityToViewPoint(m_entity.Position());
        if (mobility <= 10)
            IceObj.color = new Color(1, 1, 1, 1 - mobility / 10.0f);
        else
        {
            IceObj.color = new Color(1, 1, 1, 0);
        }

    }

    public void Update()
    {
        if (m_entity != null)
        {
            this.transform.position = m_context.EntityToViewPoint(m_entity.Position());
            energy = m_entity.Data.energy;
            mobility = m_entity.Data.Mobility;
            if (mobility <= 10)
                IceObj.color = new Color(1, 1, 1, 1 - mobility / 10.0f);
            else
            {
                IceObj.color = new Color(1, 1, 1, 0);
            }
        }
        if (Pointer != null)
            Pointer.eulerAngles = m_entity.EulerAngles;
        
    }

    protected override void Release()
    {
        IceObj.color = new Color(1, 1, 1, 0);
        m_entity = null;
        m_context = null;
    }

  


}

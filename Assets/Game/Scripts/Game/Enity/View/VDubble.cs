﻿using UnityEngine;
using System.Collections;
using System;

public class VDubble : ViewObject
{

    [SerializeField]
    private Vector3 m_entityPosition;

    private NormalDubble m_entity;
    protected override void Create(EntityObject entity)
    {
        m_entity = entity as NormalDubble;

        m_entityPosition = GameManager.Instance.Context.EntityToViewPoint(m_entity.Position());
        Vector3 pos = m_entityPosition;
        this.transform.localPosition = pos;
    }
    public void Update()
    {
        if (m_entity != null)
        {
            m_entityPosition = GameManager.Instance.Context.EntityToViewPoint(m_entity.Position());
            this.transform.position = m_entityPosition;
        }
    }

    protected override void Release()
    {
        m_entity = null;
    }

}

﻿using UnityEngine;
using System.Collections;
using System;

public class VBomb : ViewObject
{
    [SerializeField]
    private Vector3 m_entityPosition;
    [SerializeField]
    private GameObject boomEffect;
    [SerializeField]
    private GameObject boomObj;

    private NormalBomb m_entity;
    
    protected override void Create(EntityObject entity)
    {
        m_entity = entity as NormalBomb;
        m_entityPosition = m_entity.Position();
        Vector3 pos = GameManager.Instance.Context.EntityToViewPoint(m_entityPosition);
        this.transform.localPosition = pos;
    }

    public void Update()
    {
        m_entityPosition = m_entity.Position();
        Vector3 pos = GameManager.Instance.Context.EntityToViewPoint(m_entityPosition);
        this.transform.localPosition = pos;
        if(m_entity.IsBoomDuring == true&& boomEffect.activeSelf == false)
        {
            boomEffect.SetActive(true);
            boomObj.SetActive(false);
        }
    }

    protected override void Release()
    {

        boomEffect.SetActive(false);
        boomObj.SetActive(true);
    }

}

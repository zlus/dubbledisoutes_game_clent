﻿using UnityEngine;
using System.Collections.Generic;

public class NormalBomb : EntityObject
{
    private int m_type = 0;
    private Low_Vector3 startPos;
    private Low_Vector3 Pos;
    private Low_Float radians;
    private Low_Float range = GameConfig.BombRange;
    private int timer = 0;
    public bool IsBoomDuring { get; private set; }
    private int boomStartTime=40;
    private int boomDuringTime=30;
    private int endTime;
    private List<Player> BeAttachPlayerList;

    public void AddAttachPlayerList(Player entity)
    {
        BeAttachPlayerList.Add(entity);
    }

    public bool IsInAttachPlayerList(Player entity)
    {
        if (BeAttachPlayerList.Contains(entity))
            return true;
        else
            return false;
    }
    public Low_Circle Collider
    {
        get
        {
            return new Low_Circle(Pos, range);
        }
        private set
        {
        }
    }
    public void Create(int type, Low_Vector3 startPos,Low_Vector3 angle)
    {
        BeAttachPlayerList = new List<Player>();
        IsBoomDuring = false;
        m_type = type;
        this.startPos = startPos;
        Pos = startPos;
        radians =Low_Math.DegreesToRadians(angle.Z);
        endTime = boomStartTime + boomDuringTime;
        ViewFactory.CreateView(AppConfig.rootPath+"Prefabs/Bomb/bomb_" + m_type, AppConfig.rootPath+ "Prefabs/Bomb/bomb_0", this);

    }

    public void EnterFrame(int frameIndex)
    {
        timer++;
        if(timer<= boomStartTime)
        {
            Pos = new Low_Vector2(startPos.X - 3*timer * Low_Math.Sin(radians), startPos.Y + 3*timer * Low_Math.Cos(radians));
        }
        if(timer> boomStartTime&& IsBoomDuring==false&&timer<= endTime)
        {
            IsBoomDuring = true;
        }
        if(timer> endTime)
        {
            IsBoomDuring = false;
            GameManager.Instance.RemoveBomb(this);
        }
    }

    protected override void Release()
    {
        BeAttachPlayerList.Clear();
        IsBoomDuring = false;
        timer = 0;
        ViewFactory.ReleaseView(this);
    }
    public override Vector3 Position()
    {
        return Pos;
    }
}

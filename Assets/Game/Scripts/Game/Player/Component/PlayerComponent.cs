using UnityEngine;
using System.Collections;

public abstract class PlayerComponent
{
    public PlayerComponent(Player player) {

    }

    public abstract void Release();

    public abstract void EnterFrame(int frameIndex);

}

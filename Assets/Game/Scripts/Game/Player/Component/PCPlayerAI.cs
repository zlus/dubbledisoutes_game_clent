using UnityEngine;
using System.Collections;
using System;

public class PCPlayerAI : PlayerComponent
{
    private Player m_player;
    private int m_lastActionFrame = 0;

    private GameContext m_context;
    public PCPlayerAI(Player player) : base(player)
    {
        m_player = player as Player;
        m_context = GameManager.Instance.Context;

        RandomDirection();
    }
    float m_lastActionFrame2 = 0;
    float m_lastActionFrame3 = 0;
    public override void EnterFrame(int frameIndex)
    {
        float dt3 = frameIndex - m_lastActionFrame3;
        if (dt3 > 50)
        {
            if (m_player.CanHitNearEnemies())
            {
                int random = m_context.random.Range(0, 100);
                if (random < 25)
                {
                    m_player.InputVKey(GameVKey.bomb, 1);
                }
                else
                {
                    m_player.InputVKey(GameVKey.dubble, 1);
                }
                RandomDirection();
                m_lastActionFrame3 = frameIndex;
            }

        }

        float dt = frameIndex - m_lastActionFrame;
        if (dt > 150)
        {
            m_lastActionFrame = frameIndex;
            RandomDirection();
        }

        float dt2 = frameIndex - m_lastActionFrame2;
        if (dt2 > 10)
        {
            if (m_player.IsBeObstruct == true)
            {
                if (m_player.IsBeObstruct != false)
                {

                    m_player.InputVKey(GameVKey.MoveX, -beforeMoveX);
                    m_player.InputVKey(GameVKey.MoveY, -beforeMoveY);
                    RandomDirection();
                }

                m_lastActionFrame2 = frameIndex;
                m_player.IsBeObstruct = false;
            }
        }







    }

    public override void Release()
    {
        m_player = null;
    }
    float beforeMoveX = 0;
    float beforeMoveY = 0;
    private void RandomDirection()
    {
        beforeMoveX = m_context.random.Range(-1f, 1f);
        beforeMoveY = m_context.random.Range(-1f, 1f);

        m_player.InputVKey(GameVKey.MoveX, beforeMoveX);
        m_player.InputVKey(GameVKey.MoveY, beforeMoveY);
    }
}

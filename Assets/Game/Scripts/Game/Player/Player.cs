using UnityEngine;
using System.Collections.Generic;
using System;

public class Player:EntityObject
{
    private string LOG_TAG = "SnakePlayer";
    //======================================================================
    private PlayerData m_data = new PlayerData();
    private GameContext m_context;

    private Low_Vector3 m_Postion;
    private Low_Vector3 m_angles;
    private Low_Float range = GameConfig.PlayerRange;

    public Low_Circle Collider
    {
        get
        {
            return new Low_Circle(m_Postion, range);
        }
        private set
        {
        }
    }

    private List<PlayerComponent> m_listCompoent = new List<PlayerComponent>();
    public uint Id { get { return m_data.id; } }
    public PlayerData Data { get { return m_data; } }

    public Low_Vector3 EulerAngles { get { return m_angles; } }
    public PlayerBubbleData bubbleData { get { return m_data.bubbleData; } }

    public override Vector3 Position()
    {
        return m_Postion;
    }
    public void Create(PlayerData data, Vector3 pos)
    {
        LOG_TAG = LOG_TAG + "[" + data.id + "]";
        m_data = data;
        m_context = GameManager.Instance.Context;

        if (m_data.ai > 0)
        {
            var ai = new PCPlayerAI(this);
            m_listCompoent.Add(ai);
        }
        ViewFactory.CreateView(AppConfig.rootPath+ "Prefabs/Player", null, this);
        MoveTo(pos);
    }
    protected override void Release()
    {
        for (int i = 0; i < m_listCompoent.Count; ++i)
        {
            m_listCompoent[i].Release();
        }
        m_listCompoent.Clear();
        GameManager.Instance.AddFoodRandom(m_Postion, range, m_data.energyStore / 2);
        m_data = null;
        m_context = null;
        ViewFactory.ReleaseView(this);
        
    }
    public bool IsBeObstruct;
    /// <summary>
    /// �ƶ�
    /// </summary>
    /// <param name="pos"></param>
    public void MoveTo(Low_Vector3 pos)
    {
        if (pos.X < 0 || pos.X > m_context.mapSize.X) {
            IsBeObstruct = true;
            return;
        }
        if (pos.Y < 0 || pos.Y > m_context.mapSize.Y) {
            IsBeObstruct = true;
            return;
        }
        IsBeObstruct = false;
        Low_Vector3 oldPos = m_Postion;
        m_Postion = pos;
        Low_Vector3 dir = m_Postion - oldPos;
        m_angles.Z = (float)(Low_Math.Atan2(dir.Y, dir.X) * 180 / Math.PI) - 90;
    }

    /// <summary>
    /// �ռ�ʳ��
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public bool TryEatFood(NormalFood entity)
    {
        if(Low_CollisionHelp.IsCircleIntersectCircle(this.Collider, entity.Collider))
        {
            //m_data.mobility += 1;
            m_data.energy += 2;
            m_data.energyStore += 2;
            return true;
        }
        return false;
    }

    /// <summary>
    /// ��ײ����
    /// </summary>
    /// <param name="listPlayers"></param>
    /// <returns></returns>
    public Player TryHitEnemies(List<Player> listPlayers)
    {
        for(int i = 0; i < listPlayers.Count; i++)
        {
            Player other = listPlayers[i];
            if (this != other)
            {
                if (m_data.energy > 0 && other.Data.energy > 0) continue;

                if(Low_CollisionHelp.IsCircleIntersectCircle(this.Collider, other.Collider))
                {
                    if (m_data.energy <= 0) {
                        Blast();
                        return this;

                    }
                    if (other.Data.energy <= 0) {
                        other.Blast();
                        return other;
                    } 
                   
                } 
            }
        }
        return null;
    }

    public bool CanHitNearEnemies()
    {
        List<Player> listPlayers = GameManager.Instance.GetPlayerList();
        for (int i = 0; i < listPlayers.Count; i++)
        {
            Player other = listPlayers[i];
            if (this != other)
            {
                //if (m_data.energy > 0 && other.Data.energy > 0) continue;
                
                if (Low_CollisionHelp.IsCircleIntersectCircle(new Low_Circle(this.Collider.Position, this.Collider.Radius * 3), other.Collider))
                {
                    Vector2 dir = this.Position() - other.Position();
                    Debug.Log(dir);
                    Low_Float dirx = dir.x;
                    Low_Float diry = dir.y;
                    InputVKey(GameVKey.MoveX, dirx);
                    InputVKey(GameVKey.MoveY, diry);

                    return true;
                }
            }
        }
        return false;
    }
    /// <summary>
    /// �ͷ�����
    /// </summary>
    /// <returns></returns>
    public void  TrySpitDubble()
    {
        if (m_data.energy <= 0) return;
        m_data.energy -= 1;
        GameManager.Instance.AddDubble(0, m_data.id, m_Postion, m_angles);
    }
    /// <summary>
    /// �ͷ�ը��
    /// </summary>
    /// <returns></returns>
    public void  TrySpitBlom()
    {
        if (m_data.energy < 5) return;
        m_data.energy -= 5;
        GameManager.Instance.AddBomb(0, m_Postion, m_angles);
    }

    /// <summary>
    /// �Ƿ���ײ������
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public bool IsHitDubble(NormalDubble entity)
    {
        if (m_data.energy <= 0 || entity.PlayerID == m_data.id) return false;

        if (Low_CollisionHelp.IsCircleIntersectCircle(this.Collider, entity.Collider))
        {
            m_data.energy -= 2;
            m_data.energyStore += 1;
            GameManager.Instance.RemoveBubble(entity);
            return true;
        }
        return false;
    }

    /// <summary>
    /// �Ƿ���ײ��ը��
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public bool IsHitBomb(NormalBomb entity)
    {
        if (m_data.energy <= 0) return false;
        if (entity.IsBoomDuring == false) return false;
        if (Low_CollisionHelp.IsCircleIntersectCircle(this.Collider, entity.Collider))
        {
            if (entity.IsInAttachPlayerList(this)) return false;
            entity.AddAttachPlayerList(this);
            m_data.energy -= 10;
            m_data.energyStore += 5;
            return true;
        }
        return false;
    }

    public Player IsHitPlayer( Player entity )
    {

        if (entity.Data.energy > 0 && m_data.energy > 0) return null;

        if (Low_CollisionHelp.IsCircleIntersectCircle(this.Collider, entity.Collider))
        {
            if (entity.Data.energy <= 0) { return entity; }
            if (m_data.energy <= 0) { return entity; } 
        }
        return null;

    }

    private void Blast()
    {
        Debug.Log("energyStore :" + m_data.energyStore); 
        for (int i = 0; i < m_data.energyStore; i++)
        {
            Low_Vector3 pos = GetRandomPosition(this.Position(), this.range);
            //TODO

        }
        
    }

    //======================================================================
    private Vector3 GetRandomPosition(Vector3 center, int r)
    {
        int dx = m_context.random.Range(-r, r);
        int dy = m_context.random.Range(-r, r);
        center.x += dx;
        center.y += dy;
        return center;

    }
    public void InputVKey(int vkey, float arg)
    {
        bool hasHandled = false;
        hasHandled = hasHandled || DoVKey_Move(vkey, arg);
    }


    public void EnterFrame(int frameIndex)
    {
        for (int i = 0; i < m_listCompoent.Count; ++i)
        {
            m_listCompoent[i].EnterFrame(frameIndex);
        }

        HandleMove();
    }

    #region Move
    private Vector3 m_MoveDirection = new Vector3();
    private Vector3 m_InputMoveDirection = new Vector3();
    private float m_MoveSpeed = 1;
    public Vector3 MoveDirection { get { return m_MoveDirection; } }

    private bool DoVKey_Move(int vkey, float args)
    {
        switch (vkey)
        {
            case GameVKey.MoveX:
                m_InputMoveDirection.x = args;
                break;
            case GameVKey.MoveY:
                m_InputMoveDirection.y= args;
                break;
            case GameVKey.speedUp:
                m_MoveSpeed = args+1;
                break;
            case GameVKey.bomb:
                if(args==1)
                TrySpitBlom();
                break;
            case GameVKey.dubble:
                if (args == 1)
                    TrySpitDubble();
                break;
            default:
                return false;
        }
        return true;
    }

    private void HandleMove()
    {
        for (int i = 0; i < m_MoveSpeed; i++)
        {
            if (m_InputMoveDirection.magnitude > 0)
            {
                m_MoveDirection = m_InputMoveDirection;
            }

            if (m_MoveDirection.magnitude > 0)
            {
                Low_Vector3 pos = m_Postion + (Low_Vector3)(m_MoveDirection.normalized * m_data.Mobility/10f);
                MoveTo(pos);
            }
        }

    }
    #endregion
}

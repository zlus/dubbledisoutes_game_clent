using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour
{
    public static GameCamera Current;
    public static Camera MainCamera;
    public static uint FocusPlayerId = 0;

    public static void Create()
    {
        Camera c = GameObject.FindObjectOfType<Camera>();
    }
    public static void Release()
    {
        if (Current != null)
        {
            GameObject.Destroy(Current);
            Current = null;
        }
    }

    private GameContext m_context;

    private void Start()
    {
        Current = this;
        MainCamera = this.GetComponent<Camera>();
        m_context = GameManager.Instance.Context;
    }

    private void Update()
    {
        if (GameManager.Instance.IsRunning)
        {
            Player player = GameManager.Instance.GetPlayer(FocusPlayerId);
            if (player != null)
            {
                Vector3 targetPos = player.Position();
                targetPos = GameManager.Instance.Context.EntityToViewPoint(targetPos);

                Vector3 pos = this.transform.position;
                pos.x = targetPos.x;
                pos.y = targetPos.y;
                this.transform.position = pos;

            }
        }
    }
    void OnDestroy()
    {
        m_context = null;
    }
}

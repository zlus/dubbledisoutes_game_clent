using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPage : UINormalPanel {

    [SerializeField]
    private ScrollNavCompant scrollNavCompant;
    [SerializeField]
    private RectTransform[] ZoomButton;

    private int buttonCount;

    public Image avatarImage;

    public Image SoundBtn;

    public Transform itemArea;

    public Text coinText;
    public Text jewelText;
    public void RefreshPage()
    {
        if (AppManager.Instance.globalVolume <= 0)
        {
            SoundBtn.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "UI/Texture/Sound");
        }
        else
        {
            SoundBtn.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "UI/Texture/SoundOn");
        }

        itemArea.DestroyChildren();

        foreach(var tmp in UserManager.Instance.Skins)
        {
            GameObject go = Instantiate(UIRes.LoadPrefab("EquipItem")) ;
            go.transform.SetParent(itemArea);
            go.transform.localScale = new Vector3(1, 1, 1);
            EquipItem ei = go.GetComponent<EquipItem>();
            ei.SetIcon(tmp);
            if (tmp == UserManager.Instance.currentSkin)
            {
                ei.Equip();
            }
            else
            {
                ei.NonEquip();
            }
        }

        coinText.text = UserManager.Instance.Coin.ToString();
        jewelText.text = UserManager.Instance.Jewel.ToString();
    }

    protected override void OnOpen(object arg = null)
    {
        base.OnOpen(arg);
        buttonCount = ZoomButton.Length;
        scrollNavCompant.OnMoved.AddListener(OnScrolled);
        SetAvatar(UserManager.Instance.avatarId);
        RefreshPage();
    }
    private void OnScrolled(int value)
    {
        Debug.Log(value);
        for (int i = 0; i < ZoomButton.Length; i++)
        {
            if (i == value)
                ZoomButton[i].sizeDelta = new Vector2(67.7f, 140f);
            else
                ZoomButton[i].sizeDelta = new Vector2(67.7f, 70f);

        }
    }

    public void FightChose() {
        //SceneStateController.Instance.SetState(new MainScene(), "05Room");
        UIManager.Instance.OpenWindow(UIDef.FinghtChooseWnd);
    }

    public void Setting()
    {
        UIManager.Instance.OpenWindow("SettingWnd");
    }

    public void AvatarBtnClick()
    {
        UIManager.Instance.OpenWindow(UIDef.AvatarsWnd);
    }

    public void SetAvatar( int avatarIndex)
    {
        avatarImage.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "UI/Texture/Avatars/" + avatarIndex);
    }

    public void HistroyRecordBtnClick()
    {
        UIManager.Instance.OpenWindow(UIDef.HistroyWnd);
    }

    public void OnSoundClick()
    {
        if (AppManager.Instance.globalVolume > 0)
        {
            SoundBtn.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "UI/Texture/Sound");
            AppManager.Instance.globalVolume = 0;
            Sound.Instance.BgVolume = 0;
            Sound.Instance.EffectVolume = 0;

        }
        else
        {
            SoundBtn.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "UI/Texture/SoundOn");
            AppManager.Instance.globalVolume = 0.5f;
            Sound.Instance.BgVolume = 0.5f;
            Sound.Instance.EffectVolume = 0.5f;
        }
    }

}

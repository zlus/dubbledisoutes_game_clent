﻿using UnityEngine;
using System.Collections;

public static class UIDef
{
    //公用UI名
    public const string UIMsgBox = "UIMsgBox";
    public const string UIHostWnd = "UIHostWnd";
    public const string UIRoomFindWnd = "UIRoomFindWnd";
    public const string UIPVPRoomPage = "UIPVPRoomPage";
    public const string UIMainPage = "MainPage";
    public const string HistroyWnd = "HistroyWnd";
    public const string AvatarsWnd = "AvatarsWnd";
    public const string FinghtChooseWnd = "FinghtChooseWnd";
    public const string UIPVPGamePage = "UIPVPGamePage";
    public const string UIPVEGamePage = "UIPVEGamePage";
}

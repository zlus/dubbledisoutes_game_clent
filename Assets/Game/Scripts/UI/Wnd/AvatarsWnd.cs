using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarsWnd : UIWindow
{
    public void ChangeAvatars( int value )
    {
        UserManager.Instance.SetAvatarId(value);
        
        OnBtnClose();
    }
}

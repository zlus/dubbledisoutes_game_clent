﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PaySureWnd : UIWindow
{

    public System.Action m_callback;
    public Text text;
    public int _coin;

   public void SetWnd( int coin,System.Action callback )
    {
        text.text = coin.ToString();
        m_callback = callback;
        _coin = coin;
    }

    public void OnSureBtnClick()
    {
        UserManager.Instance.Coin -= _coin;
        if (UserManager.Instance.Coin < 0) UserManager.Instance.Coin = 0;
        if (m_callback!=null)
        m_callback();
        OnBtnClose();
    }
    
    public void OnCancelBtnClick()
    {
        OnBtnClose();
    }
}

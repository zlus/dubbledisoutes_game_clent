using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingWnd : UIWindow {

    public Sprite bgOpenSprite;
    public Sprite bgCloseSprite;
    public Sprite effectOpenSprite;
    public Sprite effectCloseSprite;

    public Slider slider;
    public Dropdown dropdown;

    public Image bgBtnImage;
    public Image effectBtnImage;

    protected override void OnOpen(object arg = null)
    {

        dropdown.captionText.text = LanguageManager.Instance.CurrentLanguage;
        dropdown.ClearOptions();
        List<string> langugeList = new List<string>();
        langugeList.Add(LanguageManager.Instance.GetTextContent("default.english"));
        langugeList.Add(LanguageManager.Instance.GetTextContent("default.chinese"));
        dropdown.AddOptions(langugeList);


        if (Sound.Instance.BgSoundOffOn)
        {
            bgBtnImage.sprite = bgOpenSprite;
        }
        else
        {
            bgBtnImage.sprite = bgCloseSprite;
        }

        if (Sound.Instance.EffectSoundOffOn)
        {
            effectBtnImage.sprite = effectOpenSprite;
        }
        else
        {
            effectBtnImage.sprite = effectCloseSprite;
        }

        slider.value = AppManager.Instance.globalVolume;
    }

    protected override void OnClose(object arg = null)
    {
        base.OnClose(arg);
        ModuleManager.Instance.SendMessage(ModuleDef.MainModule, MessageDef.RefreshPage);
    }

    public void OnLangugeChange()
    {
        int value = dropdown.value;
        Debug.Log(value);
        if (value == 0) LanguageManager.Instance.SetLanguage(Languages.English);
        if (value == 1) LanguageManager.Instance.SetLanguage(Languages.ChineseSimplified);

        dropdown.ClearOptions();
        List<string> langugeList = new List<string>();
        langugeList.Add(LanguageManager.Instance.GetTextContent("default.english"));
        langugeList.Add(LanguageManager.Instance.GetTextContent("default.chinese"));
        dropdown.AddOptions(langugeList);
    }

    public void OnSoundValueChange()
    {
        AppManager.Instance.globalVolume = slider.value;

        if (Sound.Instance.BgSoundOffOn)
        {
            Sound.Instance.BgVolume = AppManager.Instance.globalVolume;
        }

        if (Sound.Instance.EffectSoundOffOn)
        {
            Sound.Instance.EffectVolume = AppManager.Instance.globalVolume;
        }
    }

    public void BgSoundBtnClick()
    {
        if (Sound.Instance.BgSoundOffOn)
        {
            Sound.Instance.BgSoundOffOn = false;
            Sound.Instance.BgVolume = 0;
            bgBtnImage.sprite = bgCloseSprite;
        }
        else
        {
            Sound.Instance.BgSoundOffOn = true;
            bgBtnImage.sprite = bgOpenSprite;
            Sound.Instance.BgVolume = AppManager.Instance.globalVolume;
        }
    }

    public void EffectSoundBtnClick()
    {
        if (Sound.Instance.EffectSoundOffOn)
        {
            Sound.Instance.EffectSoundOffOn = false;
            Sound.Instance.EffectVolume = 0;
            effectBtnImage.sprite = effectCloseSprite;
        }
        else
        {
            Sound.Instance.EffectSoundOffOn = true;
            effectBtnImage.sprite = effectOpenSprite;
            Sound.Instance.EffectVolume = AppManager.Instance.globalVolume;
        }
    }




}

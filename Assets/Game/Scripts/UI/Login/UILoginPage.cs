﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UILoginPage : UINormalPanel
{
    public InputField inputId;
    public InputField inputName;

    void Start()
    {
        UserData ud = AppConfig.Value.mainUserData;
        inputName.text = ud.name;
        inputId.text = ud.id.ToString();
        ModuleManager.Instance.CreateModule(ModuleDef.LoginModule);
    }

    public void OnBtnLogin()
    {
        uint userId = 0;
        uint.TryParse(inputId.text, out userId);
        string userName = inputName.text.Trim();
        if (userId == 0)
        {
            userId= (uint)Random.Range(100000, 999999);
        }

        var module = ModuleManager.Instance.GetModule(ModuleDef.LoginModule) as LoginModule;
        if (module != null)
        {
            module.Login(userId, userName, "");
        }

    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIPVEGamePage : UINormalPanel
{
    public Text txtUserInfo;
    public ZoomButton btnReady;
    public Text txtTimeInfo;
    public Text enegyText;
    public Text speedText;
    private PVEGame m_game;

    /// <summary>
    /// 当UI打开时
    /// </summary>
    /// <param name="arg"></param>
    protected override void OnOpen(object arg)
    {
        base.OnOpen(arg);

        //需要监听游戏状态
        PVEModule module = ModuleManager.Instance.GetModule(ModuleDef.PVEModule) as PVEModule;
        m_game = module.GetCurrentGame();
        m_game.onMainPlayerDie += OnMainPlayerDie;
        m_game.onGameEnd += OnGameEnd;

        txtUserInfo.text = UserManager.Instance.MainUserData.name;
        txtTimeInfo.text = "";

    }

    protected override void OnClose(object arg)
    {
        m_game = null;
        base.OnClose(arg);
    }

    public void OnBtnReady()
    {
        UIUtils.SetActive(btnReady, false);
        m_game.CreatePlayer();
    }

    public void OnBtnPauseGame()
    {
        m_game.Pause();

        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("game.pause"),
            LanguageManager.Instance.GetTextContent("game.isExitGame"),
            LanguageManager.Instance.GetTextContent("game.exit") +"|"+ LanguageManager.Instance.GetTextContent("game.contine"), 
            (arg) =>
        {
            if (arg == null) arg = 1;
            if ((int)arg == 0)
            {
                m_game.Terminate();
               // AppManager.Instance.LoadToMainScene();
            }
            else
            {
                m_game.Resume();
            }
        });

    }

    private void OnMainPlayerDie()
    {
        m_game.Pause();

        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("game.dead") + "!!!",
            LanguageManager.Instance.GetTextContent("game.isReStartGame"+GameManager.Instance.GetPlayer(GameManager.Instance.mainId)),
            LanguageManager.Instance.GetTextContent("game.exit") + "|" + LanguageManager.Instance.GetTextContent("game.contine")
            , (arg) =>
        {
            if ((int)arg == 0)
            {
                //中止游戏
                m_game.Terminate();
            }
            else
            {
                //恢复和重生游戏
                m_game.Resume();
                m_game.RebornPlayer();
            }
        });

    }

    private void OnGameEnd()
    {
        m_game = null;
        Debug.Log("game end");
        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("game.gameOver"),
            LanguageManager.Instance.GetTextContent("game.gameCredit")+ GameManager.Instance.mainSocre,
            LanguageManager.Instance.GetTextContent("default.sure"), (arg) =>
        {
            Debug.Log("game end");
            AppManager.Instance.LoadToMainScene();
        });

    }

    private Player player;
    void Update()
    {
        if (m_game != null)
        {
            int time;
            if (m_game.IsTimelimited)
            {
                time = m_game.GetRemainTime();//second

            }
            else
            {
                time = m_game.GetElapsedTime();
            }
            player = GameManager.Instance.GetPlayer(m_game.m_mainPlayerId);
            if (player != null)
            {
                enegyText.text = player.Data.energy.ToString();
                speedText.text = player.Data.Mobility.ToString();
            }

            txtTimeInfo.text = TimeUtils.GetTimeString("%hh:%mm:%ss", time);
        }
    }

}


﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIHostWnd : UIWindow
{

    public InputField txtRoomIpPort;
    private HostModule m_module;
    public RawImage imgRoomQR;

    protected override void OnOpen(object arg = null)
    {
        Debug.Log("Open");
        base.OnOpen(arg);
        txtRoomIpPort.text = "";
        m_module = ModuleManager.Instance.GetModule(ModuleDef.HostModule)as HostModule;

        ClearRoomInfo();
        UpdateRoomInfo();
    }

    protected override void OnClose(object arg = null)
    {
        base.OnClose(arg);
        m_module = null;
    }

    public void OnBtnStartServer()
    {
        txtRoomIpPort.text = "";
        m_module.StartServer();
        UpdateRoomInfo();
    }

    public void OnBtnStopServer()
    {
        m_module.CloseServer();
        txtRoomIpPort.text = "";
        ClearRoomInfo();
    }

    //更新房间信息
    public void UpdateRoomInfo()
    {
        if (m_module.GetRoomPort() > 0)
        {
            string ipport = m_module.GetRoomIP() + ":" + m_module.GetRoomPort();
            txtRoomIpPort.text = ipport;
            imgRoomQR.enabled = true;
            Texture2D tex = QRCodeUtils.EncodeToImage(ipport, 256, 256);
            imgRoomQR.texture = tex;
        }
    }

    //清除房间信息
    private void ClearRoomInfo()
    {
        txtRoomIpPort.text = "";
        imgRoomQR.enabled = false;
    }

}

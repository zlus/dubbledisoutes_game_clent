﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EquipItem : MonoBehaviour
{
    public GameObject equipIcon;
    public GameObject nonEquipIcon;
    public Image image;
    public string m_skin;
    public void SetIcon(string skinId)
    {
        m_skin = skinId;
        image.sprite = ResManager.Instance.LoadSpriteAsset(AppConfig.rootPath + "Dubble/" + skinId);
    }

    public void EquipClick()
    {
        Debug.Log(m_skin);
        switch (m_skin)
        {
            case "item_01_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 0;
                break;
            case "item_02_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 1;
                break;
            case "item_03_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 2;
                break;
            case "item_04_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 3;
                break;
            case "item_05_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 4;
                break;
            case "item_06_02":
                UserManager.Instance.MainUserData.defaultBubbleId = 5;
                break;


        }
        Debug.Log(UserManager.Instance.MainUserData.defaultBubbleId);
        UserManager.Instance.currentSkin = m_skin;
        ModuleManager.Instance.SendMessage(ModuleDef.MainModule, MessageDef.RefreshPage);
    }

    public void Equip()
    {
        equipIcon.SetActive(true);
        nonEquipIcon.SetActive(false);
    }

    public void NonEquip()
    {
        equipIcon.SetActive(false);
        nonEquipIcon.SetActive(true);
    }
}

﻿using UnityEngine;
using System.Collections;

public class PropItem : MonoBehaviour
{

    public int coin;
    public string propId;

    public void OnClick()
    {
        PaySureWnd wnd = UIManager.Instance.OpenWindow("PaySureWnd") as PaySureWnd;
        wnd.SetWnd(coin, () =>
        {
            UserManager.Instance.Skins.Add(propId);
            ModuleManager.Instance.SendMessage(ModuleDef.MainModule, MessageDef.RefreshPage);
            Destroy(gameObject);
        });
    }
}

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIRoomFindWnd : UIWindow
{
    public InputField inputIPPort;
    public Text txtScanResult;

    private Texture2D m_TexImage;
    private string m_ImageInfo;

    protected override void OnOpen(object arg = null)
    {
        base.OnOpen(arg);
    }
    public void OnBtnOK()
    {
        //this.Close(inputIPPort.text);
        OnBtnClose(inputIPPort.text);
    }

    public void OnBtnCancel()
    {
        OnBtnClose(null);
       /// this.Close(null);
    }
    private void Update()
    {
        txtScanResult.text = m_ImageInfo;
    }
    public void OnBtnScan()
    {
//#if UNITY_EDITOR
//        m_ImageInfo = "正在打开相册";
//        StreamManager.LoadFileDialog(FolderLocations.Pictures, 512, 512,
//            new string[] { ".png", ".jpg", ".jpeg" }, OnImageLoaded);
//#else
//            m_ImageInfo = "正在打开摄像机";
//            StreamManager.LoadCameraPicker(CameraQuality.Med, 512, 512, OnImageLoaded);
//#endif
    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SGF.Network.FSPLite.Server.Data;
using System;
using System.Collections.Generic;
using SGF.UI.Component;

public class UIPVPRoomPage : UINormalPanel
{
    public ZoomButton btnJoinRoom;
    public ZoomButton btnRoomReady;
    public UIList ctlRoomPlayerList;

    private PVPRoom m_room;
    private PVPRoom GetRoom()
    {
        PVPModule module = ModuleManager.Instance.GetModule(ModuleDef.PVPModule) as PVPModule;
        return module.GetRoom();
    }

    protected override void OnOpen(object arg = null)
    {
        base.OnOpen(arg);
        m_room = GetRoom();
        m_room.OnUpdateRoomInfo += OnRoomUpdate;
        ctlRoomPlayerList.SetData(m_room.players);

        ModuleManager.Instance.Event(ModuleDef.HostModule, "onStartServer").AddListener(onStartServer);
        ModuleManager.Instance.Event(ModuleDef.HostModule, "onCloseServer").AddListener(onCloseServer);
    }

    protected override void OnClose(object arg = null)
    {
        ModuleManager.Instance.Event(ModuleDef.HostModule, "onStartServer").RemoveListener(onStartServer);
        ModuleManager.Instance.Event(ModuleDef.HostModule, "onCloseServer").RemoveListener(onCloseServer);

        if (m_room != null)
        {
            m_room.OnUpdateRoomInfo -= OnRoomUpdate;
            m_room = null;
        }
        base.OnClose(arg);
    }

    private void onStartServer(object arg0)
    {
        string ipport = arg0 as string;
        string[] tmps = ipport.Split(':');
        string ip = tmps[0];
        int port = int.Parse(tmps[1]);
        m_room.JoinRoom(ip, port);

    }

    private void onCloseServer(object arg0)
    {
        m_room.ExitRoom();
    }

    public void OnBtnJoinRoom()
    {
        if (UIUtils.GetButtonText(btnJoinRoom) == LanguageManager.Instance.GetTextContent("game.enterRoom"))
        {
            UIWindow wnd = UIManager.Instance.OpenWindow(UIDef.UIRoomFindWnd);

            wnd.RegisterOnClose = (arg) =>
            {
                string str = arg as string;
                if (string.IsNullOrEmpty(str))
                {
                    return;
                }
                string[] tmps = str.Split(':');
                if (tmps.Length != 2)
                {
                    return;
                }
                string ip = tmps[0];
                int port = int.Parse(tmps[1]);
                m_room.JoinRoom(ip, port);
            };
        }
        else
        {
            m_room.ExitRoom();
        }
    }
    public void OnBtnRoomReady()
    {
        if (UIUtils.GetButtonText(btnRoomReady) == LanguageManager.Instance.GetTextContent("game.startReady"))
        {
            m_room.RoomReady();
        }
        else
        {
            m_room.CancelReady();
        }
    }

    public void OnBtnShowHost()
    {
        ModuleManager.Instance.ShowModule(ModuleDef.HostModule);
    }

    public void OnBtnGoBack()
    {
        PVPModule module = ModuleManager.Instance.GetModule(ModuleDef.PVPModule) as PVPModule;
        if (m_room.IsInRoom)
        {
            m_room.ExitRoom();
        }
        module.CloseRoom();
        AppManager.Instance.LoadToMainScene();
    }

    public void OnBtnTest()
    {
        List<FSPPlayerData> list = new List<FSPPlayerData>();
        for(int i = 0; i < 20; ++i)
        {
            FSPPlayerData data = new FSPPlayerData();
            data.name = "名字";
            data.id = (uint)i + 1;
            data.userId = (uint)GameRandom.Default.Range(100000, 999999);
            data.sid = data.id;
            data.isReady = GameRandom.Default.Rnd() > 0.5f;
            list.Add(data);

        }
        ctlRoomPlayerList.SetData(list);
    }

    private void OnRoomUpdate(FSPRoomData data)
    {
        ctlRoomPlayerList.SetData(data.players);
    }
    void Update()
    {
        m_room = GetRoom();
        if (m_room != null)
        {
            UIUtils.SetButtonText(btnJoinRoom, m_room.IsInRoom ? LanguageManager.Instance.GetTextContent("game.exitRoom") : LanguageManager.Instance.GetTextContent("game.enterRoom"));
            UIUtils.SetButtonText(btnRoomReady, m_room.IsReady ? LanguageManager.Instance.GetTextContent("game.cancelready") : LanguageManager.Instance.GetTextContent("game.startReady"));

            UIUtils.SetActive(btnJoinRoom, !m_room.IsReady);
            UIUtils.SetActive(btnRoomReady, m_room.IsInRoom);
        }
    }



}

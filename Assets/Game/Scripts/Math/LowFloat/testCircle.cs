﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testCircle : MonoBehaviour {

    public Transform circleView;
    public Transform circleView1;
    public Transform rectView;

    Low_Circle circle;
    Low_Circle circle1;
    Low_Rect rect;
    public void Start()
    {
        circle = new Low_Circle(circleView.transform.position, UnityUtil.GetSpriteTrueSize(circleView.gameObject).x/2);
        circle1 = new Low_Circle(circleView1.transform.position, UnityUtil.GetSpriteTrueSize(circleView1.gameObject).x / 2);
        rect = new Low_Rect(rectView.position, UnityUtil.GetSpriteTrueSize(rectView.gameObject));
        Low_CollisionHelp.IsCircleIntersectRect(circle, rect);
    }

    public void Update()
    {
        //circle.Posiotion = circleView.position;
        //rect.Position = rectView.position;
        //if (Low_CollisionHelp.IsCircleIntersectRect(circle, rect))
        //{
        //    Debug.Log("发生碰撞");
        //}
        //else
        //{
        //    Debug.Log("未发生碰撞");
        //}
        circle.Position = circleView.position;
        circle1.Position = circleView1.position;
        if (Low_CollisionHelp.IsCircleIntersectCircle(circle, circle1))
        {
            Debug.Log("发生碰撞");
        }
        else
        {
            Debug.Log("未发生碰撞");
        }
    }

}

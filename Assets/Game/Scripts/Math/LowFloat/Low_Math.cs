﻿using UnityEngine;
using System.Collections;
using System;

public class Low_Math
{
    public static Low_Float Pow(Low_Float v)
    {
        return v * v;
    }
    public static Low_Float Fabs(Low_Float v)
    {
        return Mathf.Abs(v.Value);
    }

    public static Low_Float Sqrt(Low_Float v)
    {
        return Mathf.Sqrt(v);
    }
    public static Low_Float PI()
    {
        return 3.1415926;
    }

    public static Low_Float Sin(float Angle)
    {
        return Math.Sin(Angle);
    }
    public static Low_Float Cos(float Angle)
    {
        return Math.Cos(Angle);
    }

    internal static Low_Float Atan2(Low_Float y, Low_Float x)
    {
        return Mathf.Atan2(y, x);
    }
    public static Low_Float DegreesToRadians(int Degrees)
    {
        return 0.01745329f * Degrees;
    }
}

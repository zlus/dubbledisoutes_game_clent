﻿
using UnityEngine;
public struct  Low_Circle
{
    private Low_Vector2 m_posiotion;
    private Low_Float m_radius;
    public Low_Vector2 Position { get { return m_posiotion; } set { m_posiotion = value; } }
    public Low_Float Radius { get { return m_radius; } set { m_radius = value; } }
    public Low_Circle(Low_Vector2 position,Low_Float radius ) {

        m_posiotion = position;
        m_radius = radius;

    }
    public bool Contains(Low_Vector2 point)
    {
        if(Low_Math.Pow(point.X - m_posiotion.X) + Low_Math.Pow(point.Y - m_posiotion.Y) > m_radius)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
﻿using System;
using UnityEngine;
public struct Low_Float
{
    private float m_Value;
    public float Value { get { return m_Value; } }

    public Low_Float(float value)
    {
        m_Value=MathUtil.ToLowFloat(value);
    }

    #region 运算符重载

    public static Low_Float operator -(Low_Float x)
    {
        return -x.Value;
    }
    public static Low_Float operator + (Low_Float x,Low_Float y)
    {
        return new Low_Float(MathUtil.ToLowFloat(x.Value + y.Value));
    }

    public static Low_Float operator -(Low_Float x, Low_Float y) {
        return new Low_Float(x.Value - y.Value);
    }

    public static Low_Float operator *(Low_Float x, Low_Float y)
    {
        return new Low_Float(x.Value * y.Value);
    }

    public static Low_Float operator /(Low_Float x, Low_Float y)
    {
        return new Low_Float(MathUtil.ToLowFloat(x.Value / y.Value));
    }

    public static bool operator >(Low_Float x, Low_Float y)
    {
        return x.Value > y.Value;
    }
    public static bool operator <(Low_Float x, Low_Float y)
    {
        return x.Value < y.Value;
    }
    public static bool operator >=(Low_Float x, Low_Float y)
    {
        return x.Value >= y.Value;
    }
    public static bool operator <=(Low_Float x, Low_Float y)
    {
        return x.Value <= y.Value;
    }
    #endregion

    #region 隐式转换 
    public static implicit operator Low_Float(float v)
    {
       // m_Value = MathUtil.ToLowFloat(v);
        return new Low_Float(v) ;
    }

    public static implicit operator Low_Float(double v)
    {
        return new Low_Float((float)v);
    }

    public static implicit operator Low_Float(int v)
    {
        return new Low_Float(v);
    }

    public static implicit operator float(Low_Float v)
    {
        return v.Value;
    }
    public static implicit operator double(Low_Float v)
    {
        return v.Value;
    }
    public static implicit operator int(Low_Float v)
    {
        return (int)v.Value;
    }
    #endregion
    public override string ToString()
    {
        return m_Value.ToString();
    }
}

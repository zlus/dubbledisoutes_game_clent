﻿using UnityEngine;
using System.Collections;



public class Low_Rect
{
    public Low_Rect(Low_Vector2 position, Low_Vector2 size)
    {
        Position = position;
        Size = size;
    }
    LowRectType m_RectType;
    public LowRectType RectType {

        get {
            return m_RectType;
        }
        set {
            m_RectType = value;
            

        }

    }
    public Low_Vector2 Center { get {
            switch (m_RectType)
            {
                case LowRectType.CENTER:
                    return Position;
                case LowRectType.UP:
                    return new Low_Vector2(Position.X, Position.Y + Size.Y / 2);
                case LowRectType.BOTTOM:
                    return new Low_Vector2(Position.X, Position.Y - Size.Y / 2);
                case LowRectType.LEFT:
                    return new Low_Vector2(Position.X - Size.X / 2, Position.Y);
                case LowRectType.RIGHT:
                    return new Low_Vector2(Position.X + Size.X / 2, Position.Y);
                default:
                    return Position;
            }
        }
    }
    public Low_Vector2 Position { get; set; }
    public Low_Vector2 Size { get; set; }

    public enum LowRectType
    {
        CENTER,
        UP,
        BOTTOM,
        LEFT,
        RIGHT
    }

}

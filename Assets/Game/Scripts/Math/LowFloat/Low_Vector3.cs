using UnityEngine;
public struct Low_Vector3
{
    private Low_Float m_x;
    private Low_Float m_y;
    private Low_Float m_z;
    public Low_Float X { get { return m_x; } set { m_x = value; } }
    public Low_Float Y { get { return m_y; } set { m_y = value; } }
    public Low_Float Z { get { return m_z; } set { m_z = value; } }
    public Low_Vector3(Low_Float x, Low_Float y,Low_Float z)
    {
        m_x = x;
        m_y = y;
        m_z = z;
    }


    public static Low_Vector3 operator +(Low_Vector3 v1, Low_Vector3 v2)
    {
        return new Low_Vector3(v1.X + v2.X, v1.Y + v2.Y,v1.Z+v2.Z);
    }
    public static Low_Vector3 operator -(Low_Vector3 v1, Low_Vector3 v2)
    {
        return new Low_Vector3(v1.X - v2.X, v1.Y - v2.Y,v1.Z-v2.Z);
    }
    public static Low_Vector3 operator -(Low_Vector3 v)
    {
        return new Low_Vector3(-v.X, -v.Y,-v.Z);
    }
    public static Low_Vector3 operator /(Low_Vector3 v1, int v2)
    {
        return new Low_Vector3(v1.X/2, v1.Y/2, v1.Z/2);
    }

    public static implicit operator Low_Vector3(Vector2 v)
    {
        return new Low_Vector3(v.x, v.y,0);
    }
    public static implicit operator Low_Vector3(Vector3 v)
    {
        return new Low_Vector3(v.x, v.y,v.z);
    }

    public static implicit operator Low_Vector3(Low_Vector2 v)
    {
        return new Low_Vector3(v.X, v.Y,0);
    }

    public static implicit operator Vector2(Low_Vector3 v)
    {
        return new Vector2(v.X, v.Y);
    }

    public static implicit operator Vector3(Low_Vector3 v)
    {
        return new Vector3(v.X, v.Y,v.Z);
    }

    public static implicit operator Low_Vector2(Low_Vector3 v)
    {
        return new Low_Vector2(v.X, v.Y);
    }
}

﻿using UnityEngine;
using System.Collections;

public static class Low_CollisionHelp
{
    
    /// <summary>
    /// 圆和矩形的碰撞检测
    /// 发生交叉碰撞返回true,否则false
    /// </summary>
    /// <param name="circle">圆</param>
    /// <param name="rect">矩形</param>
    /// <returns></returns>
    public static bool IsCircleIntersectRect(Low_Circle circle, Low_Rect rect)
    {
        //Low_Vector2 upCenterPoint = rect.Center + new Low_Vector2(0, rect.Size.Y / 2.0f);
        //Low_Vector2 RightCenterPoint = rect.Center + new Low_Vector2(rect.Size.X / 2.0f, 0);
        Low_Float r = circle.Radius;
        //Low_Float w1 = DistanceBetweenTwoPoint_NonSqrt(rect.Position, RightCenterPoint);
        //Low_Float h1 = DistanceBetweenTwoPoint_NonSqrt(rect.Position, upCenterPoint);
        //Low_Float w2 = DistanceFromPointToLine_NonSqrt(circle.Posiotion, rect.Position, upCenterPoint);
        //Low_Float h2 = DistanceFromPointToLine_NonSqrt(circle.Posiotion, rect.Position, RightCenterPoint);
        //Debug.Log("xx" + " " +rect.Size);
        //Debug.Log("rect point" + " " + rect.Position);
        //Debug.Log("circle point" + " " + circle.Posiotion);
        //rect point[x:-5.34999  y: 1.01999]
        //circle point[x:-3  y: 2.67]                                                
        //6.36055 5.35461 1.74647 2.00551
        Low_Float w1 = rect.Size.X / 2;
        Low_Float h1 = rect.Size.Y / 2;
        Low_Float w2 = Low_Math.Fabs((circle.Position.X - rect.Center.X));
        Low_Float h2 = Low_Math.Fabs((circle.Position.Y - rect.Center.Y));
        if (w2 > w1 + r)
            return false;
        if (h2 > h1 + r)
            return false;
        if (w2 <= w1)
            return true;
        if (h2 <=h1)
            return true;

        return Low_Math.Pow(w2 - w1) + Low_Math.Pow(h2 - h1) <= Low_Math.Pow(r);

    }
    /// <summary>
    /// 圆和圆的碰撞检测
    /// </summary>
    /// <param name="c1">圆1</param>
    /// <param name="c2">圆2</param>
    /// <returns></returns>
    public static bool IsCircleIntersectCircle(Low_Circle c1, Low_Circle c2)
    {
        return DistanceBetweenTwoPoint_NonSqrt(c1.Position, c2.Position) <= c1.Radius + c2.Radius;
       
    }


    //========================================计算有问题。。。暂时不需要==========================================
    /// <summary>
    /// 计算两点间的距离
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    public static Low_Float DistanceBetweenTwoPoint_NonSqrt(Low_Vector2 p1, Low_Vector2 p2)
    {
        return Low_Math.Sqrt(Low_Math.Pow( p1.X - p2.X) + Low_Math.Pow(p1.Y - p2.Y));
    }
    /// <summary>
    /// 计算点到线的距离
    /// </summary>
    /// <param name="point"></param>
    /// <param name="linePoint1"></param>
    /// <param name="linePoint2"></param>
    /// <returns></returns>
    public static Low_Float DistanceFromPointToLine_NonSqrt(Low_Vector2 point, Low_Vector2 linePoint1, Low_Vector2 linePoint2)
    {
        Low_Float a = linePoint2.Y - linePoint1.Y;
        Low_Float b = linePoint1.X - linePoint2.X;
        Low_Float c = linePoint2.X * linePoint1.Y - linePoint1.X * linePoint2.Y;
        return Low_Math.Fabs(a * point.X + b * point.Y + c) / Mathf.Sqrt((Low_Math.Pow(a) + Low_Math.Pow(b)));
    }

}

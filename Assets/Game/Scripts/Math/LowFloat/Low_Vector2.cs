﻿using UnityEngine;
public struct Low_Vector2
{
    private Low_Float m_x;
    private Low_Float m_y;
    public Low_Float X { get { return m_x; } set { m_x = value; } }
    public Low_Float Y { get { return m_y; } set { m_y = value; } }
    public Low_Vector2(Low_Float x, Low_Float y) {
        m_x = x;
        m_y = y;
    }
    public static Low_Vector2 operator +(Low_Vector2 v1, Low_Vector2 v2)
    {
        return new Low_Vector2(v1.X+v2.X,v1.Y+v2.Y);
    }
    public static Low_Vector2 operator -(Low_Vector2 v1, Low_Vector2 v2)
    {
        return new Low_Vector2(v1.X - v2.X, v1.Y - v2.Y);
    }

    public static implicit operator Low_Vector2(Vector2 v)
    {
        return new Low_Vector2(v.x, v.y);
    }
    public static implicit operator Low_Vector2(Vector3 v)
    {
        return new Low_Vector2(v.x, v.y);
    }
    public static implicit operator Vector2(Low_Vector2 v)
    {
        return new Vector2(v.X, v.Y);
    }
    public static implicit operator Vector3(Low_Vector2 v)
    {
        return new Vector3(v.X, v.Y);
    }
    public override string ToString()
    {
        return string.Format("[x:{0}  y:{1}]",m_x,m_y );
    }
}
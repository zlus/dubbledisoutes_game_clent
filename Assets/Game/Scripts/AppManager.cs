using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AppManager : ServiceModule<AppManager> {

    public float globalVolume = 0.5f;

    public override void Init()
    {
        base.Init();
        NetworkManager.Instance.Start();
        
    }

    public void LoadToMainScene()
    {
        SceneManager.LoadScene("04Main");
        SceneManager.sceneLoaded += sceneLoaded;
        Sound.Instance.PlayBg("Bg1");
    }

    private void sceneLoaded(Scene scene, LoadSceneMode loadMode)
    {

        UIManager.Instance.ClearAll();

        if(scene.name== "04Main")
        {
            ModuleManager.Instance.ReleaseAll();
            // UIManager.Instance.OpenNormalPanel("MainPage");
            ModuleManager.Instance.CreateModule(ModuleDef.MainModule);
            ModuleManager.Instance.ShowModule(ModuleDef.MainModule);
        }
        if(scene.name== "05Room")
        {
            ModuleManager.Instance.ReleaseAll();
            ModuleManager.Instance.CreateModule(ModuleDef.PVPModule);
            ModuleManager.Instance.CreateModule(ModuleDef.HostModule);
            ModuleManager.Instance.ShowModule(ModuleDef.PVPModule, (int)GameMode.EndlessPVP);
        }

    }

    public void LoadToRoomScene()
    {

        SceneManager.sceneLoaded -= sceneLoaded;
        SceneManager.LoadScene("05Room");
        SceneManager.sceneLoaded += sceneLoaded;
        Sound.Instance.PlayBg("Bg2");
    }

    public void LoadToFightScene()
    {

    }

}

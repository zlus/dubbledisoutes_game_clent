using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Game :GameSystem {
    public override void Awake()
    {
        //LanguageManager.Instance.currentlanguage = sysdefine.languages.implifiedchinese;
        DontDestroyOnLoad(this.gameObject);
        ResManager.Instance.LoadAssetBundle("all");
        Sound.Instance.Init();
        LanguageManager.Instance.Init();
        LanguageManager.Instance.SetLanguage(Languages.English);
        SceneManager.LoadScene(1);
    }

    public override void FixedUpdate()
    {

    }

    public override void OnApplicationQuit()
    {

    }

    public override void OnDestroy()
    {

    }

    public override void Start()
    {

    }

    public override void Update()
    {

    }
}

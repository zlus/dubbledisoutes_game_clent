using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class UserManager : ServiceModule<UserManager>
{
    public int avatarId = 0;
    private UserData m_mainUserData;
    public UserData MainUserData { get { return m_mainUserData; } }

    public List<string> Skins=new List<string>();

    public string currentSkin;
    public void UpdateMainUserData(UserData data)
    {
        m_mainUserData = data;
    }

    public int Coin = 300;
    public int Jewel = 20;
    public override void Init()
    {

        //UserData data = new UserData();
        //data.id = 1;
        //data.level = 2;
        //data.name = "xxx";
        //data.defaultBubbleId = 0;

        //UserManager.Instance.UpdateMainUserData(data);

        avatarId = PlayerPrefs.GetInt("avatarId", avatarId);
        Skins.Add("item_01_02");
        currentSkin = "item_01_02";
    }

    public void SetAvatarId(int id)
    {
        avatarId = id;
        PlayerPrefs.SetInt("avatarId", avatarId);
        ModuleManager.Instance.SendMessage(ModuleDef.MainModule, MessageDef.ChangeAvatar, id);
    }
}

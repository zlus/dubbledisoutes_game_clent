using ProtoBuf;
[ProtoContract]
public class UserData
{
    [ProtoMember(1)]
    public uint id;
    [ProtoMember(2)]
    public string name;
    [ProtoMember(3)]
    public int level;
    [ProtoMember(4)]
    public int defaultBubbleId;
}

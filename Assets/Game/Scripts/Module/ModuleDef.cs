using UnityEngine;
using System.Collections;

public static class ModuleDef { 
    
    public const string HostModule = "HostModule";
    public const string PVPModule = "PVPModule";
    public const string MainModule = "MainModule";
    public const string LoginModule = "LoginModule";
    public const string PVEModule = "PVEModule";
}

public static class MessageDef
{
    public const string ChangeAvatar = "ChangeAvatar";
    public const string RefreshPage = "RefreshPage";
}

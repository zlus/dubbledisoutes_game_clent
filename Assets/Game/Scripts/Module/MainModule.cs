﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MainModule : BusinessModule
{
    private MainPage ui_MainPange;

  

    public override void Create(object args = null)
    {
        base.Create(args);
    }

    protected override void Show(object arg)
    {
        base.Show(arg);
        ui_MainPange =UIManager.Instance.OpenNormalPanel("MainPage") as MainPage;
    }



    public override void Release()
    {
        base.Release();
        UIManager.Instance.CloseNormalPanel("MainPage");
    }

    protected override void OnModuleMessage(string msg, object[] args)
    {
        base.OnModuleMessage(msg, args);
        if (msg == MessageDef.ChangeAvatar)
        {
            int index = (int)args[0];
            ui_MainPange.SetAvatar(index);
        }
        if (msg == MessageDef.RefreshPage)
        {
            ui_MainPange.RefreshPage();
        }
    }
}

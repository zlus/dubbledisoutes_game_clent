using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PVEModule : BusinessModule {

    private PVEGame m_game;

    protected override void Show(object arg)
    {
        base.Show(arg);
        int model = (int)arg;

        //TODO ��ʾ�ؿ�
        StartGame(model);
    }

    private void StartGame(int mode)
    {
        GameParam param = new GameParam();
        param.mode = (GameMode)mode;
        param.limitedTime = 180;

        m_game = new PVEGame();
        m_game.Start(param);
        m_game.onGameEnd += () =>
        {
            StopGame();
        };
        UIManager.Instance.OpenNormalPanel(UIDef.UIPVEGamePage);
    }

    private void StopGame()
    {
        if (m_game != null)
        {
            m_game.Stop();
            m_game = null;
        }
    }
    public PVEGame GetCurrentGame()
    {
        return m_game;
    }
}

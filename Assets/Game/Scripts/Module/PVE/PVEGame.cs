using System;
public class PVEGame
{
    public uint m_mainPlayerId = 1;
    private int m_frameIndex = 0;
    private bool m_pause = false;

    public event Action onMainPlayerDie;
    public event Action onGameEnd;
    private GameContext m_context;

    public void Start(GameParam param)
    {

        GameManager.Instance.CreateGame(param);
        GameManager.Instance.onPlayerDie += onPlayerDie;
        m_context = GameManager.Instance.Context;

        PlayerData pd = new PlayerData();
        pd.id = m_mainPlayerId;
        pd.userId = UserManager.Instance.MainUserData.id;
        pd.bubbleData.id = UserManager.Instance.MainUserData.defaultBubbleId;
        pd.ai = 0;
        GameManager.Instance.RegPlayerData(pd);

        GameInput.Create();
        GameInput.OnVkey += OnVKey;

        MonoHelper.AddFixedUpdateListener(FixedUpdate);
        GameCamera.FocusPlayerId = m_mainPlayerId;
    }

    /// <summary>
    /// 停止游戏
    /// </summary>
    public void Stop()
    {
        MonoHelper.RemoveFixedUpdateListener(FixedUpdate);

        GameInput.Release();

        GameManager.Instance.ReleaseGame();

        onGameEnd = null;
        onMainPlayerDie = null;
        m_context = null;
    }

    public void Pause()
    {
        m_pause = true;
    }
    public void Resume()
    {
        m_pause = false;
    }
    /// <summary>
    /// 中止游戏
    /// </summary>
    public void Termiate() {
        Pause();
        if (onGameEnd != null)
            onGameEnd();
    }

    /// <summary>
    /// 创建玩家
    /// </summary>
    public void CreatePlayer()
    {
        GameManager.Instance.InputVKey(GameVKey.CreatePlayer, 0, m_mainPlayerId);
    }
    /// <summary>
    /// 重生玩家
    /// </summary>
    public void RebornPlayer()
    {
        CreatePlayer();
    }
    /// <summary>
    /// 收到GameInput的输入
    /// </summary>
    /// <param name="vkey"></param>
    /// <param name="arg"></param>
    private void OnVKey(int vkey,float arg)
    {
        GameManager.Instance.InputVKey(vkey, arg, m_mainPlayerId);
    }
    
    private void FixedUpdate()
    {
        if (m_pause)
        {
            return;
        }
        m_frameIndex++;
        GameManager.Instance.EnterFrame(m_frameIndex);
        CheckTimeEnd();

    }

    public bool IsTimelimited
    {
        get
        {
            return m_context.param.mode == GameMode.TimelimitPVE;
        }
    }

    private void CheckTimeEnd()
    {
        if( IsTimeLimited)
        {
            if (GetRemainTime() <= 0)
            {
                Termiate();
            }
        }
    }

    public bool IsTimeLimited
    {
        get
        {
            return m_context.param.mode == GameMode.TimelimitPVE;
        }
    }
    /// <summary>
    /// 游戏剩余时间
    /// </summary>
    /// <returns></returns>
    public int GetRemainTime()
    {
        if (m_context.param.mode == GameMode.TimelimitPVE)
        {
            return (int)(m_context.param.limitedTime - m_context.currentFrameIndex * 0.033333333);
        }
        return 0;
    }
    /// <summary>
    /// 游戏经历时间
    /// </summary>
    /// <returns></returns>
    public int GetElapsedTime()
    {
        return (int)(m_context.currentFrameIndex * 0.033333333f);
    }

    /// <summary>
    /// 玩家死亡
    /// </summary>
    /// <param name="playerId"></param>
    private void onPlayerDie(uint playerId)
    {
        if (m_mainPlayerId == playerId)
        {
            Pause();
            if (onMainPlayerDie != null)
            {
                onMainPlayerDie();
            }
            else
            {
               // this.LogError("OnPlayerDie() onMainPlayerDie==null!");
            }
        }
    }
    public void Terminate()
    {
        Pause();

        if (onGameEnd != null)
        {
            onGameEnd();
        }
    }
}
    










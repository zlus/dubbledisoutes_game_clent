using UnityEngine;
using System.Collections;
using SGF.Network.FSPLite.Client;
using System.Collections.Generic;
using System;
using SGF.Network.FSPLite;

public class PVPGame
{
    public string LOG_TAG = "PVPGame";

    private FSPManager m_mgrFSP;
    private List<PlayerData> m_listPlayerData;
    public uint m_mainPlayerId;

    public event Action onMainPlayerDie;
    public event Action onGameEnd;
    private GameContext m_context;
    
    //-----------------------------------------------
    /// <summary>
    /// 开始游戏
    /// </summary>
    /// <param name="param"></param>
    public void Start(PVPStartParam param)
    {

        UserData mainUserData = UserManager.Instance.MainUserData;
        m_listPlayerData = param.players;
        for (int i = 0; i < m_listPlayerData.Count; i++)
        {
            if (m_listPlayerData[i].userId == mainUserData.id)
            {
                m_mainPlayerId = m_listPlayerData[i].id;
                GameCamera.FocusPlayerId = m_mainPlayerId;
                GameManager.Instance.mainId = m_mainPlayerId;
            }

            //注册玩家数据，为在帧同步过程中创建玩家提供数据
            GameManager.Instance.RegPlayerData(m_listPlayerData[i]);
        }

        //启动游戏逻辑
        GameManager.Instance.CreateGame(param.gameParam);
        GameManager.Instance.onPlayerDie += OnPlayerDie;//有玩家死亡
        m_context = GameManager.Instance.Context;

        //启动帧同步逻辑
        m_mgrFSP = new FSPManager();
        m_mgrFSP.Start(param.fspParam, m_mainPlayerId);
        m_mgrFSP.SetFrameListener(OnEnterFrame);
        m_mgrFSP.onGameBegin += OnGameBegin;//游戏开始
        m_mgrFSP.onGameExit += OnGameExit;//有玩家退出
        m_mgrFSP.onRoundEnd += OnRoundEnd;//有玩家退出
        m_mgrFSP.onGameEnd += OnGameEnd;//游戏结束


        //初始化输入
        GameInput.Create();
        GameInput.OnVkey += OnVKey;
       // UIAPI.ShowMsgBox("test", "test foe", "sdf");
        //监听EnterFrame
        MonoHelper.AddFixedUpdateListener(FixedUpdate);

        
        GameBegin();
    }

    /// <summary>
    /// 停止游戏 
    /// </summary>
    public void Stop()
    {

        GameManager.Instance.ReleaseGame();

        MonoHelper.RemoveFixedUpdateListener(FixedUpdate);
        GameInput.Release();

        if (m_mgrFSP != null)
        {
            m_mgrFSP.Stop();
            m_mgrFSP = null;
        }

        onMainPlayerDie = null;
        onGameEnd = null;
        m_context = null;
    }
    /// <summary>
    /// 中止游戏
    /// </summary>
    public void GameExit()
    {
        //因为PVP 模式中，还有其它玩家在玩，
        //所以应该只是让自己退出游戏
        m_mgrFSP.SendGameExit();
    }
    private void GameBegin()
    {
        m_mgrFSP.SendGameBegin();
    }

    private void OnGameBegin(int obj)
    {
        RoundBegin();
    }

    public void RoundBegin()
    {
        m_mgrFSP.SendRoundBegin();
    }
    public void RoundEnd()
    {
        m_mgrFSP.SendRoundEnd();
    }
    private void OnRoundEnd(int obj)
    {
        GameEnd();
    }

    private void GameEnd()
    {
        m_mgrFSP.SendGameEnd();
    }


    /// <summary>
    /// 创建玩家
    /// </summary>
    public void CreatePlayer()
    {
        m_mgrFSP.SendFSP(GameVKey.CreatePlayer);
    }

    /// <summary>
    /// 重生玩家
    /// </summary>
    public void RebornPlayer()
    {
        m_mgrFSP.SendFSP(GameVKey.CreatePlayer);
    }
    //------------------------------------------------------------------

    /// <summary>
    /// 来自GameInput的输入
    /// </summary>
    private void OnVKey(int vkey, float arg)
    {
        m_mgrFSP.SendFSP(vkey, (int)(arg * 10000));
    }

    private void FixedUpdate()
    {

        //向服务器tick
        m_mgrFSP.EnterFrame();
    }

    private void OnEnterFrame(int frameId, FSPFrame frame)
    {
        
        GameManager.Instance.EnterFrame(frameId);
        Debug.Log(frameId);
        if (frame != null && frame.vkeys != null)
        {
            for (int i = 0; i < frame.vkeys.Count; i++)
            {
                FSPVKey cmd = frame.vkeys[i];
                Debug.Log(cmd.ToString());
                GameManager.Instance.InputVKey(cmd.vkey, cmd.args[0]*1.0f / 10000.0f, cmd.playerId);
            }
        }

        CheckTimeEnd();
    }
    /// <summary>
    /// 检测是否限时结束
    /// </summary>
    private void CheckTimeEnd()
    {
        if (IsTimelimited)
        {
            if (GetRemainTime() <= 0)
            {
                RoundEnd();
            }
        }
    }
    /// <summary>
    /// 是否限时模式
    /// </summary>
    public bool IsTimelimited
    {
        get
        {
            return m_context.param.mode == GameMode.TimelimitPVP;
        }
    }

    /// <summary>
    /// 如果是限时模式，还剩下多少时间
    /// </summary>
    /// <returns></returns>
    public int GetRemainTime()
    {
        if (m_context.param.mode == GameMode.TimelimitPVP)
        {
            return (int)(m_context.param.limitedTime - m_context.currentFrameIndex * 0.033333333);
        }
        return 0;
    }

    /// <summary>
    /// 游戏已经经过多少时间
    /// </summary>
    /// <returns></returns>
    public int GetElapsedTime()
    {
        return (int)(m_context.currentFrameIndex * 0.033333333f);
    }


    private void OnGameEnd(int obj)
    {
        if (onGameEnd != null)
        {
            onGameEnd();
        }
    }



    private void OnGameExit(uint playerId)
    {
        //当退出的是自己时
        if (m_mainPlayerId == playerId)
        {
            if (onGameEnd != null)
            {
                onGameEnd();
            }
        }
    }


    private void OnPlayerDie(uint playerId)
    {
        //当死亡的自己时
        if (m_mainPlayerId == playerId)
        {
            if (onMainPlayerDie != null)
            {
                onMainPlayerDie();
            }
        }
    }

}

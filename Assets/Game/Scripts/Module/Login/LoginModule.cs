using UnityEngine;
using UnityEngine.UI;
public class LoginModule:BusinessModule
{
    protected override void Show(object arg)
    {
        
    }

    public void Login(uint id, string name, string pwd)
    {
        //由于我们暂时没有服务端，所以这里并不真正向服务器发协议
        //而是假设已经收到服务端登录成功的协议
        UserData ud = new UserData();
        ud.id = id;
        ud.name = name;
        ud.defaultBubbleId = 1;

        //假设登录成功了
        OnLoginSuccess(ud);

    }

    private void OnLoginSuccess(UserData ud)
    {
        UserManager.Instance.UpdateMainUserData(ud);

        AppConfig.Value.mainUserData = UserManager.Instance.MainUserData;
        AppConfig.Save();

        AppManager.Instance.LoadToMainScene();
    }

}
using UnityEngine;
using System.Collections;
using SGF.Network.FSPLite.Server;
using SGF;
using SGF.Network.FSPLite;

public class HostModule : BusinessModule
{

    private ModuleEvent onStartServer;
    private ModuleEvent onCloseServer;

    public override void Create(object args)
    {
        base.Create(args);
        onStartServer = Event("onStartServer");
        onCloseServer = Event("onCloseServer");
    }

    protected override void Show(object arg)
    {
        Debug.Log("显示主机的窗口");
        //显示主机的窗口
        UIManager.Instance.OpenWindow(UIDef.UIHostWnd);
    }

    public void StartServer()
    {
        FSPServer.Instance.Start(0);
        GameParam gameParam = new GameParam();
        byte[] customGameParam = PBSerializer.NSerialize(gameParam);
        //将自定义游戏参数传给房间
        FSPServer.Instance.Room.SetCustomGameParam(customGameParam);
        FSPServer.Instance.SetServerTimeout(0);

        string ipport = GetRoomIP() + ":" + GetRoomPort();
        onStartServer.Invoke(ipport);
    }

    /// <summary>
    /// 关闭服务器
    /// </summary>
    public void CloseServer()
    {
        FSPServer.Instance.Close();
        onCloseServer.Invoke(null);
    }

    /// <summary>
    /// 房间IP
    /// </summary>
    /// <returns></returns>
    public string GetRoomIP()
    {
        return FSPServer.Instance.RoomIP;
    }
    /// <summary>
    /// 房间Port
    /// </summary>
    /// <returns></returns>
    public int GetRoomPort()
    {
        return FSPServer.Instance.RoomPort;
    }

    /// <summary>
    /// 帧同步参数
    /// </summary>
    /// <returns></returns>
    public FSPParam GetFSPParam()
    {
        return FSPServer.Instance.GetParam();
    }
}

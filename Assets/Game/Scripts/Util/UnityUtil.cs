using UnityEngine;
public  class UnityUtil
{
    public static Vector2 GetSpriteTrueSize(GameObject sprite)
    {
        Vector3 size = sprite.GetComponent<SpriteRenderer>().sprite.bounds.size;
        //计算缩放后的真正尺寸
        size.x *= sprite.transform.localScale.x;
        size.y *= sprite.transform.localScale.y;
        size.z *= sprite.transform.localScale.z;

        return size;
    }
}

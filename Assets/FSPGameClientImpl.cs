﻿using SGF.Network.FSPLite;
using SGF.Network.FSPLite.Client;
using SGF.Network.FSPLite.Server;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FSPGameClientImpl:MonoBehaviour
{
    private FSPManager m_mgrFSP;
    public string LOG_TAG = "FSPGameClientImpl";
    public uint m_playerId;
    private string m_lastVKey;

    public FSPGameClientImpl(uint playerId)
    {
        m_playerId = playerId;
        LOG_TAG = LOG_TAG + "[" + m_playerId + "]";
    }
    public void SendStart()
    {
        if (FSPServer.Instance.IsRunning)
        {

            m_Start(FSPServer.Instance.GetParam());

        }
    }

    public void SendRoundBegin() {

        Debug.Log("rB");
        m_mgrFSP.SendRoundBegin();

    }
    public void SendControlStart() {
        m_mgrFSP.SendControlStart();
    }

    public void SendPing() {
        m_mgrFSP.SendFSP(FSPVKeyBase.PING, GameRandom.Default.Range(1, 1000));
    }

    public void SendRoundEnd() {
        m_mgrFSP.SendRoundEnd();
    }

    public void SendGameEnd()
    {
        m_mgrFSP.SendGameEnd();
    }
    public void SendGameExit() {
        m_mgrFSP.SendGameExit();
    }

    public Text text;
    public Text kText;

    public void Update()
    {

        if (m_mgrFSP != null)
        {
            text.text = "Client[" + m_playerId + "]: " + m_mgrFSP.GameState;
        }
        else
        {
            text.text = "Client[" + m_playerId + "]";
        }
        kText.text = "最近一次VKey：" + m_lastVKey;

    }


    public void m_Start(FSPParam param)
    {
        this.Log("Start()");
        param.sid = m_playerId;
        m_mgrFSP = new FSPManager();
        m_mgrFSP.Start(param, m_playerId);
        m_mgrFSP.SetFrameListener(OnEnterFrame);
        m_mgrFSP.onGameEnd += (arg) =>
        {
            this.Log("OnGameEnd() " + arg);
            DelayInvoker.DelayInvoke(0.01f, Close);
            //Close();
        };
        m_mgrFSP.onGameExit += (playerId) =>
        {
            this.Log("onGameExit() " + playerId);
            if (playerId == m_playerId)
            {
                DelayInvoker.DelayInvoke(0.01f, Close);
                //Close();
            }
        };

        m_mgrFSP.SendGameBegin();
    }

    public void Close(params object[] args)
    {
        this.Log("Close()");

        if (m_mgrFSP != null)
        {
            m_mgrFSP.Stop();
            m_mgrFSP = null;
        }
    }

    public void SendVKey(int vkey, int arg)
    {
        m_mgrFSP.SendFSP(vkey, arg);
    }

    public void fixedUpdate()
    {
        if (m_mgrFSP != null)
        {
            m_mgrFSP.EnterFrame();
        }
    }

    private void OnEnterFrame(int frameId, FSPFrame frame)
    {
        if (frame != null && frame.vkeys != null)
        {
            for (int i = 0; i < frame.vkeys.Count; i++)
            {
                FSPVKey cmd = frame.vkeys[i];
                m_lastVKey = cmd.ToString();
                this.Log("OnEnterFrame() frameId:{0}, cmd:{1}", frameId, cmd.ToString());
            }
        }
    }




}

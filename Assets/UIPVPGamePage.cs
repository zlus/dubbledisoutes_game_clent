using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIPVPGamePage : UINormalPanel
{
    public Text txtUserInfo;
    public ZoomButton btnReady;
    public Text txtTimeInfo;
    public Text enegyText;
    public Text speedText;
    public PVPGame m_game;

    protected override void OnOpen(object arg = null)
    {
        base.OnOpen(arg);
        PVPModule module = ModuleManager.Instance.GetModule(ModuleDef.PVPModule) as PVPModule;
        m_game = module.GetGame();
        m_game.onMainPlayerDie += OnMainPlayerDie;
        m_game.onGameEnd += OnGameEnd;
        txtUserInfo.text = UserManager.Instance.MainUserData.name;
        txtTimeInfo.text = "";
    }

    protected override void OnClose(object arg = null)
    {
        m_game = null;
        base.OnClose(arg);
    }

    public void OnBtnReady()
    {
        UIUtils.SetActive(btnReady, false);
        m_game.CreatePlayer();
    }

    private void OnMainPlayerDie()
    {
        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("game.dead") + "!!!",
            LanguageManager.Instance.GetTextContent("game.isReStartGame"),
            LanguageManager.Instance.GetTextContent("game.exit") + "|" + LanguageManager.Instance.GetTextContent("game.contine")
            , (arg) => {
            if ((int)arg == 0)
            {
                m_game.GameExit();
            }
            else
            {
                m_game.RebornPlayer();
            }
        });
    }

    public void OnBtnSet()
    {
        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("default.Setting"),
            LanguageManager.Instance.GetTextContent("game.isExitGame"),
           LanguageManager.Instance.GetTextContent("game.exit") + "|" + LanguageManager.Instance.GetTextContent("game.contine")
           , (arg) =>
        {
            if ((int)arg == 0)
            {
                m_game.GameExit();
            }
            else
            {
               // UIManager.Instance.CloseWindow();
            }
        });
    }

    private void OnGameEnd()
    {
        m_game = null;
        UIAPI.ShowMsgBox(LanguageManager.Instance.GetTextContent("game.gameOver"),
            LanguageManager.Instance.GetTextContent("game.gameCredit" ) + GameManager.Instance.mainSocre,
            LanguageManager.Instance.GetTextContent("default.sure"), (arg) =>
            {
            AppManager.Instance.LoadToMainScene();
        });
    }
    private Player player;
    private void Update()
    {
        if (m_game != null)
        {
            int time = 0;
            if (m_game.IsTimelimited)
            {
                time = m_game.GetRemainTime();
            }
            else
            {
                time = m_game.GetElapsedTime();
            }
            player = GameManager.Instance.GetPlayer(m_game.m_mainPlayerId);
            if (player != null)
            {
                enegyText.text = player.Data.energy.ToString();
                speedText.text = player.Data.Mobility.ToString();
            }
            txtTimeInfo.text = TimeUtils.GetTimeString("%hh:%mm:%ss", time);
        }
    }
}

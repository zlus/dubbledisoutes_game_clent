﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SGF.Network.FSPLite.Client;
using SGF.Network.FSPLite.Server;
using UnityEngine;
using UnityEngine.UI;
namespace SGF.Network.FSPLite.Example
{
    public class Example_FSPLite:MonoBehaviour
    {
        public FSPGameClientImpl m_client1 = new FSPGameClientImpl(1);
        public FSPGameClientImpl m_client2 = new FSPGameClientImpl(2);
        private FSPParam m_fspParam;
        void Start()
        {
            Debuger.EnableLog = true;
        }

        public Text Servertext;
        public Text ClientText1;
        public Text ClientText2;

        public void StartStartServer() {
            FSPServer.Instance.Start(0);
            FSPServer.Instance.SetFrameInterval(66, 2);
            FSPServer.Instance.SetServerTimeout(0);
            FSPServer.Instance.StartGame();
            FSPServer.Instance.Game.AddPlayer(1, 1);
            FSPServer.Instance.Game.AddPlayer(2, 2);
            m_fspParam = FSPServer.Instance.GetParam();
        }

        public void CloseServer() {

            FSPServer.Instance.Close();

        }

        void Update()
        {
            
            if (FSPServer.Instance.IsRunning)
            {
                Servertext.text = string.Format("Server IP  :{0}  Server Port: {1}", m_fspParam.host, m_fspParam.port);
            }
        }

        void FixedUpdate()
        {
            m_client1.fixedUpdate();
            m_client2.fixedUpdate();
        }
    }


   


}

